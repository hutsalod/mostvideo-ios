//
//  ReceiptItemAdapter.swift
//  MostVideo
//
//  Created by Roman on 07.12.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

//import Foundation
//import SwiftyStoreKit
//
//class ReceiptInfoAdapter {
//    var parsedInAppPurchaseReceipt: ParsedInAppPurchaseReceipt
//    
//    init(parsedInAppPurchaseReceipt: ParsedInAppPurchaseReceipt) {
//        self.parsedInAppPurchaseReceipt = parsedInAppPurchaseReceipt
//    }
//    
//    func getReceiptInfo() -> ReceiptInfo {
//        var receiptInfo: ReceiptInfo = [:]
//        receiptInfo["product_id"] = parsedInAppPurchaseReceipt.productIdentifier as AnyObject
//        receiptInfo["quantity"] = parsedInAppPurchaseReceipt.quantity as AnyObject
//        receiptInfo["transaction_id"] = parsedInAppPurchaseReceipt.transactionIdentifier as AnyObject
//        receiptInfo["original_transaction_id"] = parsedInAppPurchaseReceipt.originalTransactionIdentifier as AnyObject
//        receiptInfo["web_order_line_item_id"] = parsedInAppPurchaseReceipt.webOrderLineItemId as AnyObject
//        if let purchaseDateSec = parsedInAppPurchaseReceipt.purchaseDate?.timeIntervalSince1970 {
//            receiptInfo["purchase_date_ms"] = (purchaseDateSec * 1000) as AnyObject
//        }
//        if let purchaseDateSec = parsedInAppPurchaseReceipt.originalPurchaseDate?.timeIntervalSince1970 {
//            receiptInfo["original_purchase_date_ms"] = (purchaseDateSec * 1000) as AnyObject
//        }
//        if let expiresDateSec = parsedInAppPurchaseReceipt.subscriptionExpirationDate?.timeIntervalSince1970 {
//            receiptInfo["expires_date_ms"] = (expiresDateSec * 1000) as AnyObject
//        }
//        if let cancellationDateSec = parsedInAppPurchaseReceipt.cancellationDate?.timeIntervalSince1970 {
//            receiptInfo["cancellation_date_ms"] = (cancellationDateSec * 1000) as AnyObject
//        }
//        print(receiptInfo)
//        print(parsedInAppPurchaseReceipt)
//        return receiptInfo
//    }
//}
