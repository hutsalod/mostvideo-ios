//
//  IAPManager.swift
//  MostVideo
//
//  Created by Roman on 07.12.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import SwiftyStoreKit
import RealmSwift

let productIds: Set<String> = ["tv.mostvideo.app.sub.sevendays"]

//func durationInDays(productId: String) -> Int? {
//    switch productId {
//    case "tv.mostvideo.app.sub.sevendays":
//        return 7
//    default:
//        return nil
//    }
//}

class IAPManager: NSObject {
    static let shared = IAPManager()
    
    override init() {
        super.init()
    }
    
    public func verifyValidAllSubscriptions(productIds: Set<String>, completion: @escaping (Bool) -> Void) {
        UserProfileManager.getUserProfile { getUserProfileResult in
            if let dateTo = Realm.current?.objects(UserProfile.self).first?.subscriptions.first?.dateTo,
                Date().timeIntervalSince1970 < dateTo.timeIntervalSince1970 {
                completion(true)
            } else {
                let receiptValidator = ReceiptValidator()
                let result = receiptValidator.validateReceipt()
                switch result {
                case .success:
                    let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: "9d36a822cc7e4853a80465e25c44a9b3")
                    SwiftyStoreKit.verifyReceipt(using: appleValidator, forceRefresh: false) { result in
                        switch result {
                        case .success(let receipt):
                            print("Online verify receipt success: \(receipt)")
                            let verifySubscriptionsResult = SwiftyStoreKit.verifySubscriptions(ofType: .nonRenewing(validDuration: 7 * 24 * 3600), productIds: productIds , inReceipt: receipt)
                            switch verifySubscriptionsResult {
                            case .purchased:
                                completion(true)
                            default:
                                completion(false)
                            }
                        case .error(let error):
                            print("Verify receipt failed: \(error)")
                            completion(false)
                        }
                    }
                case .error(let error):
                    print("Local validation is failed \(error)")
                    completion(false)
                }
            }
        }
    }
    
//    private func getSubTypeId(for duration: Int) -> Int? {
//        return Realm.current?.objects(Subscription.self).filter("duration = \(duration)").first?.id
//    }
    
    public func sendValidSubscriptionToServer(productIds: Set<String>) {
        let receiptValidator = ReceiptValidator()
        let result = receiptValidator.validateReceipt()
        switch result {
        case .success:
            let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: "9d36a822cc7e4853a80465e25c44a9b3")
            SwiftyStoreKit.verifyReceipt(using: appleValidator, forceRefresh: false) { result in
                switch result {
                case .success(let receipt):
                    print("send: Online verify receipt success: \(receipt)")
                    let verifySubscriptionsResult = SwiftyStoreKit.verifySubscriptions(ofType: .nonRenewing(validDuration: 7 * 24 * 3600), productIds: productIds, inReceipt: receipt)
                    switch verifySubscriptionsResult {
                    case .purchased(_, let items):
                        for item in items {
                            switch item.productId {
                            case "tv.mostvideo.app.sub.sevendays":
                                let dateTo = item.purchaseDate.addingTimeInterval(3600 * 24 * 7)
                                let subTypeId = 371657
                                    SubscriptionManager.addSubscription(subTypeId: subTypeId, originalTransactionId: item.originalTransactionId, dateFrom: item.purchaseDate, dateTo: dateTo, completion: { (_) in
                                    })
                            default:
                                break
                            }
                        }
                    default:
                        break
                    }
                case .error(let error):
                    print("send: Verify receipt failed: \(error)")
                }
            }
        case .error(let error):
            print("send: Local validation is failed \(error)")
        }
    }
}
