//
//  ProgramGuideCell.swift
//  MostVideo
//
//  Created by Roman on 16.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import RealmSwift

class ProgramGuideCell: UITableViewCell {
    
    static let identifier = "ProgramGuideCell"

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var noticeButton: UIButton! {
        didSet {
            noticeButton.setImage(UIImage(named: "Notice-Active"), for: .selected)
            noticeButton.setImage(UIImage(named: "Notice-Normal"), for: .normal)
        }
    }
    
    private var isRequesting = false
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    var programGuide: ProgramGuide? {
        didSet {
            guard programGuide?.validated() != nil else { return }
            updateProgramGuideInformation()
        }
    }
    
    func reset() {
        timeLabel.text = nil
        categoryLabel.text = nil
        nameLabel.text = nil
        descLabel.text = nil
        noticeButton.isSelected = false
        isRequesting = false
    }
    
    @IBAction func noticeButtonAction(_ sender: UIButton) {
        guard let programGuide = self.programGuide else { return }
        
        if programGuide.forceNotice {
            Alert(key: "error.delete_notice_only_live").present()
            return
        }
        
        guard !isRequesting else { return }
        let state = noticeButton.isSelected
        noticeButton.isSelected = !state
        isRequesting = true
        ProgramGuideManager.setProgramNotice(programId: programGuide.id, isActice: !state) { [weak self] result in
            self?.isRequesting = false
            if result {
                Realm.executeOnMainThread({ _ in
                    programGuide.notice = !state
                })
            } else {
                self?.noticeButton.isSelected = state
            }
        }
    }
    
    func updateProgramGuideInformation() {
        guard let programGuide = self.programGuide else { return }
        
        if !programGuide.categoryName.isEmpty {
            categoryLabel.text = programGuide.categoryName
        } else {
            categoryLabel.text = nil
        }
        
        if let date = programGuide.date {
            timeLabel.text = date.getTimeStringForProgramGuide()
            timeLabel.textColor = date < Date() ? #colorLiteral(red: 0.5600000024, green: 0.5600000024, blue: 0.5600000024, alpha: 1) : #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
            nameLabel.textColor = date < Date() ? #colorLiteral(red: 0.5600000024, green: 0.5600000024, blue: 0.5600000024, alpha: 1) : #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        }
        
        nameLabel.text = programGuide.name
        
        descLabel.text = programGuide.desc
        
        noticeButton.isSelected = programGuide.notice || programGuide.forceNotice
    }
}
