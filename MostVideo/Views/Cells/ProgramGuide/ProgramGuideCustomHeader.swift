//
//  ProgramGuideCustomHeader.swift
//  MostVideo
//
//  Created by Roman on 22.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit

class ProgramGuideCustomHeader: UITableViewHeaderFooterView {
    
    static let identifier = "ProgramGuideCustomHeader"
    @IBOutlet weak var dateLabel: UILabel!
    
}
