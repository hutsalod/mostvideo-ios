//
//  AllSibscriptionsCell.swift
//  MostVideo
//
//  Created by Roman on 21.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit

class AllSibscriptionsCell: UITableViewCell {
    
    static let identifier = "AllSibscriptionsCell"

    @IBOutlet weak var nameLabel: UILabel!
    
    var subscription: IAPSubscription? {
        didSet {
            updateSubscriptionInformation()
        }
    }
    
    func reset() {
        nameLabel.text = nil
    }
    
    func updateSubscriptionInformation() {
        guard let subscription = self.subscription else { return }
        nameLabel.text = "\(subscription.localizedDescription) (\(subscription.localizedPrice))"
    }
    
}
