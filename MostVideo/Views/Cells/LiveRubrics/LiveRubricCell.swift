//
//  LiveRubricCell.swift
//  MostVideo
//
//  Created by Roman on 20.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import RealmSwift

class LiveRubricCell: UITableViewCell {
    
    static let identifier = "LiveRubricCell"

    @IBOutlet weak var rubricLabel: UILabel!
    @IBOutlet weak var rubricSwitch: UISwitch!
    
    private var isRequesting = false
    
    var rubric: Rubric? {
        didSet {
            guard rubric?.validated() != nil else { return }
            updateRubricInformation()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func reset() {
        rubricLabel.text = nil
        rubricSwitch.isOn = false
    }
    
    @IBAction func rubricSwitchChanged(_ sender: UISwitch) {
        guard !isRequesting else { return }
        guard let rubric = self.rubric else { return }
        let state = rubricSwitch.isOn
        isRequesting = true
        
        UserProfileManager.setLiveRubric(id: rubric.id, isSelected: state) { [weak self] result in
            self?.isRequesting = false
            if result {
                self?.rubricSwitch.isOn = state
                Realm.executeOnMainThread({ _ in
                    rubric.checked = state
                })
            } else {
                self?.rubricSwitch.isOn = !state
            }
            NotificationCenter.default.post(name: Notification.Name("UpdateProgramGuide"), object: nil, userInfo: nil)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateRubricInformation() {
        guard let rubric = self.rubric else { return }
        rubricLabel.text = rubric.name
        rubricSwitch.isOn = rubric.checked
    }
    
}
