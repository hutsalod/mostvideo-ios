//
//  MyDeviceCell.swift
//  MostVideo
//
//  Created by Roman on 21.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit

class MyDeviceCell: UITableViewCell {
    
    static let identifier = "MyDeviceCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var dateLastLoginLabel: UILabel!
    
    var device: Device? {
        didSet {
            guard device?.validated() != nil else { return }
            updateDiviceInformation()
        }
    }

    func reset() {
        nameLabel.text = nil
        idLabel.text = nil
        dateLastLoginLabel.text = nil
    }
    
    func updateDiviceInformation() {
        guard let device = self.device else { return }
        nameLabel.text = device.deviceName
        idLabel.text = device.deviceId
        dateLastLoginLabel.text = device.date?.getDateStringForDeviceList()
    }
    
}
