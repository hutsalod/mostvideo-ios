//
//  ArchiveAdapter.swift
//  MostVideo
//
//  Created by Mac Book  on 06.07.2020.
//  Copyright © 2020 MostVideo. All rights reserved.
//

import UIKit

struct ArchiveAdapter{
    var image: String
    var title: String
    var abode: String
    var imageVideo: UIImage
    var url: String
}
