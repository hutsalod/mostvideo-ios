//
//  ArchiveCell.swift
//  MostVideo
//
//  Created by Mac Book  on 02.07.2020.
//  Copyright © 2020 MostVideo. All rights reserved.
//

import UIKit

class ArchiveCell: UITableViewCell {
    

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var dateLastLoginLabel: UILabel!
    @IBOutlet weak var imageVideo: UIImageView!
    
    func setUsers(user: ArchiveAdapter) {
        idLabel.layer.masksToBounds = true
        idLabel.layer.cornerRadius = 12.0
        nameLabel.text = user.image
        idLabel.text = user.abode
        dateLastLoginLabel.text = user.title
        imageVideo.image = user.imageVideo

    }
   

}
