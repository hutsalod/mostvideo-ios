//
//  CurrentSubscriptionCell.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit

class CurrentSubscriptionCell: UITableViewCell {
    
    static let identifier = "CurrentSubscriptionCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    var currentSubscription: CurrentSubscription? {
        didSet {
            guard currentSubscription?.validated() != nil else { return }
            updateCurrentSubscriptionInformation()
        }
    }
    
    func reset() {
        nameLabel.text = nil
        durationLabel.text = nil
    }
    
    
    func updateCurrentSubscriptionInformation() {
        guard let currentSubscription = self.currentSubscription else { return }
        
        nameLabel.text = "\(currentSubscription.name)"
        if let _ = currentSubscription.dateFrom, let dateTo = currentSubscription.dateTo {
            durationLabel.text = "по \(dateTo.getDateStringForCurrentSubscriprion())"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
