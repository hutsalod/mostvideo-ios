//
//  ArchiveModel.swift
//  MostVideo
//
//  Created by Mac Book  on 04.08.2020.
//  Copyright © 2020 MostVideo. All rights reserved.
//
import Foundation
import RealmSwift
import SwiftyJSON

class ArchiveModel: Object{
    @objc dynamic var image: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var abode: String = ""
    
    @objc dynamic var imageVideo: Data? = nil
    @objc dynamic var url: String = ""
}
