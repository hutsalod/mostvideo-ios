//
//  Auth.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/7/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

final class Auth: Object {
    @objc dynamic var token: String?
    @objc dynamic var lastAccess: Date?
}
