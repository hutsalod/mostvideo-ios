//
//  Workout.swift
//  MostVideo
//
//  Created by Roman on 15.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation

final class Workout: BaseModel {

    @objc dynamic var name = ""
    @objc dynamic var day: Int = 0
    @objc dynamic var hour: Int = 0
    @objc dynamic var checked: Bool = false
}

