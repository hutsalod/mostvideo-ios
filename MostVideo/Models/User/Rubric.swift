//
//  Rubric.swift
//  MostVideo
//
//  Created by Roman on 14.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation

final class Rubric: BaseModel {

    @objc dynamic var name = ""
    @objc dynamic var checked: Bool = false
}


