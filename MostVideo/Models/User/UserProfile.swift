//
//  User.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/7/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

final class UserProfile: BaseModel {
    @objc dynamic var name: String?
    @objc dynamic var lastName: String?
    @objc dynamic var phone: String?
    @objc dynamic var email: String?
    @objc dynamic var hdVideo: Bool = false
    @objc dynamic var allRubrics: Bool = false
    var liveRubrics = List<Rubric>()
    @objc dynamic var fiveMin: Bool = false
    @objc dynamic var morningWorkoutsActive: Bool = false
    var morningWorkouts = List<Workout>()
    var subscriptions = List<CurrentSubscription>()
    var devices = List<Device>()
}

