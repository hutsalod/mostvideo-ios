//
//  Subscription.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation

final class CurrentSubscription: BaseModel {
    @objc dynamic var dateFrom: Date?
    @objc dynamic var dateTo: Date?
    @objc dynamic var name: String = ""
}
