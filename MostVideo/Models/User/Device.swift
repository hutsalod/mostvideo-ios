//
//  Device.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift

final class Device: Object {
    
    @objc dynamic var date: Date?
    @objc dynamic var deviceId: String = ""
    @objc dynamic var deviceName: String = ""
    
    
    override static func primaryKey() -> String? {
        return "deviceId"
    }
    
    
    func validated() -> Self? {
        guard !isInvalidated else {
            return nil
        }
        return self
    }
    
}
