//
//  Subscription.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation

final class Subscription: BaseModel {
    @objc dynamic var name = ""
    @objc dynamic var duration: Int = 0
    @objc dynamic var cost: String = ""
}

