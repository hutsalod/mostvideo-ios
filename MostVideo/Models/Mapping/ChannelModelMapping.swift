//
//  ChannelModelMapping.swift
//  MostVideo
//
//  Created by Roman on 15.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

extension Channel: ModelMappeable {
    func map(_ values: JSON, realm: Realm?) {
        
        if let id = values["ID"].int {
            self.id = id
        }
        
        if let name = values["NAME"].string {
            self.name = name
        }
        
        if let code = values["CODE"].string {
            self.code = code
        }
        
        if let imageHolder = values["PICTURE"].string {
            self.imageHolder = imageHolder
        }
        
        if let streamUrl = values["STREAMSRC"].string {
            self.streamUrl = streamUrl
        }
        
        if let streamHDUrl = values["STREAMHDSRC"].string {
            self.streamHDUrl = streamHDUrl
        }
        
        
        if let programmUrl = values["PROGRAMM"].string {
            self.programmUrl = programmUrl
        }
        
        
    }
}
