//
//  UserModelMapping.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 13/01/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

extension UserProfile: ModelMappeable {
    func map(_ values: JSON, realm: Realm?) {

        if let name = values["NAME"].string {
            self.name = name
        }
        
        if let lastName = values["LAST_NAME"].string {
            self.lastName = lastName
        }
        
        if let phone = values["PERSONAL_PHONE"].string {
            self.phone = phone
        }
        
        if let email = values["EMAIL"].string {
            self.email = email
        }
        
        if let hdVideo = values["VIDEO_HD"].bool {
            self.hdVideo = hdVideo
        }
        
        if let allRubrics = values["ALL_RUBRICS"].bool {
            self.allRubrics = allRubrics
        }
        
        if let liveRubricsRaw = values["LIVE_RUBRICS"].array {
            let liveRubrics = liveRubricsRaw.compactMap { (liveRubricRaw) -> Rubric? in
                let rubric = Rubric(value: [
                    "id": liveRubricRaw["ID"].intValue,
                    "name": liveRubricRaw["NAME"].stringValue,
                    "checked": liveRubricRaw["CHECKED"].boolValue
                    ])
                guard rubric.id > 0, !rubric.name.isEmpty else { return nil }
                
                let livePrograms = ProgramGuide.findAll(by: rubric.id)
                Realm.executeOnMainThread { _ in
                    livePrograms?.setValue(rubric.checked, forKey: "forceNotice")
                }
                return rubric
            }
            self.liveRubrics.removeAll()
            self.liveRubrics.append(objectsIn: liveRubrics)
        }
        
        if let fiveMin = values["FIVEMIN"].bool {
            self.fiveMin = fiveMin
        }
        
        if let morningWorkoutsActive = values["MORNING_WORKOUT_ACTIVE"].bool {
            self.morningWorkoutsActive = morningWorkoutsActive
        }
        
        if let workoutsRaw = values["MORNING_WORKOUT"].array {
            let workouts = workoutsRaw.compactMap { workoutRaw -> Workout? in
                let workout = Workout(value: [
                    "id": workoutRaw["ID"].intValue,
                    "name": workoutRaw["NAME"].stringValue,
                    "day": workoutRaw["DW"].intValue,
                    "hour": workoutRaw["TH"].intValue,
                    "checked": workoutRaw["CHECKED"].boolValue
                    ])
                guard workout.id > 0,
                    !workout.name.isEmpty,
                    workout.day >= 1, workout.day <= 7,
                    workout.hour > 0
                    else { return nil }
                return workout
            }
            self.morningWorkouts.removeAll()
            self.morningWorkouts.append(objectsIn: workouts)
        }
        
        if let subscriptionsRaw = values["USER_SUBSCRIPTION"].array {
            let subscriptions = subscriptionsRaw.compactMap { subscriptionRaw -> CurrentSubscription? in
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
                let subscription = CurrentSubscription(value: [
                    "id": subscriptionRaw["ID"].intValue,
                    "dateFrom": dateFormatter.date(from: subscriptionRaw["DATE_ACTIVE_FROM"].stringValue) ?? Date(),
                    "dateTo": dateFormatter.date(from: subscriptionRaw["DATE_ACTIVE_TO"].stringValue) ?? Date(),
                    "name": Subscription.find(withIdentifier: subscriptionRaw["ID"].intValue)?.name ?? ""
                    ])
                guard subscription.id > 0
                    else { return nil }
                return subscription
            }
            self.subscriptions.removeAll()
            self.subscriptions.append(objectsIn: subscriptions)
        }
        
        if let devicesRaw = values["DEV_LIST_AUTH"].array {
            let devices = devicesRaw.compactMap { deviceRaw -> Device? in
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
                let device = Device(value: [
                    "date": dateFormatter.date(from: deviceRaw["DATE_CREATE"].stringValue) ?? Date(),
                    "deviceId": deviceRaw["DEVICE_ID"].stringValue,
                    "deviceName": deviceRaw["DEVICE_NAME"].stringValue
                    ])
                guard !device.deviceName.isEmpty
                    else { return nil }
                return device
            }
            self.devices.removeAll()
            
            self.devices.append(objectsIn: devices)
        }
    }

}
