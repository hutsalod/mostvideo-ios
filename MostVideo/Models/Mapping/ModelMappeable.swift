//
//  ModelMappeable.swift
//  MostVideo
//
//  Created by Roman on 13.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift
import Realm

protocol ModelMappeable {
    func map(_ values: JSON, realm: Realm?)
}
