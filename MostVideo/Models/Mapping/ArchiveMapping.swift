//
//  ArchiveMapping.swift
//  MostVideo
//
//  Created by Mac Book  on 04.08.2020.
//  Copyright © 2020 MostVideo. All rights reserved.
//



import Foundation
import SwiftyJSON
import RealmSwift

extension ArchiveModel {
    func map(_ values: JSON, realm: Realm?) {

        if let url = values["URL"].string {
            self.url = url
        }
        
        if let name = values["NAME"].string {
            self.title = name
        }
        
        if let duration = values["PREVIEW_PIC"].string {
            
           let url =  URL(string: duration)!
            self.imageVideo =  try? Data(contentsOf: url)
        }
       
        if let cost = values["COST"].string {
            self.abode = cost
        }
    }
}
