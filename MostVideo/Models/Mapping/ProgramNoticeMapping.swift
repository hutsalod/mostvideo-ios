//
//  ProgramNoticeMapping.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

extension ProgramNotice: ModelMappeable {
    func map(_ values: JSON, realm: Realm?) {
        
        if let programId = values["PID"].int {
            self.programId = programId
        }
        
        if let isActive = values["ACTIVE"].bool {
            self.isActive = isActive
        }
    }
}
