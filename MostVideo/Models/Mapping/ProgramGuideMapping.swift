//
//  ProgramGuideMapping.swift
//  MostVideo
//
//  Created by Roman on 16.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import SWXMLHash
import RealmSwift

extension ProgramGuide {
    
    func deserialize(programmUrlSting: String, values: XMLIndexer) {
        
        if let idStr = values["ID"].element?.text, let id = Int(idStr) {
            self.id = id
        }
        
        if let dateAsString = values["datep"].element?.text {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            self.date = dateFormatter.date(from: dateAsString)
        }
        
        if let name = values["Name"].element?.text {
            self.name = name
        }
        
        if let desc = values["Description"].element?.text {
            self.desc = desc
        }
        
        if let durationStr = values["Duration"].element?.text, let duration = Int(durationStr) {
            self.duration = duration
        }
        
        if let categoryIdStr = values["streamcat"].element?.text, let categoryId = Int(categoryIdStr) {
            self.categoryId = categoryId
        }
        
        if let categoryName = values["bcasttype"].element?.text {
            self.categoryName = categoryName
        }
        
        self.programmUrlString = programmUrlSting
        
    }
}

