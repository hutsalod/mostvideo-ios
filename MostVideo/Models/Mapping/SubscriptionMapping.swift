//
//  SubscriptionMapping.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

extension Subscription: ModelMappeable {
    func map(_ values: JSON, realm: Realm?) {

        if let id = values["ID"].int {
            self.id = id
        }
        
        if let name = values["NAME"].string {
            self.name = name
        }
        
        if let duration = values["DURATION"].int {
            self.duration = duration
        }
       
        if let cost = values["COST"].string {
            self.cost = cost
        }
    }
}
