//
//  ProgramGuide.swift
//  MostVideo
//
//  Created by Roman on 16.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

final class ProgramGuide: BaseModel {
    @objc dynamic var date: Date? = nil
    @objc dynamic var name: String = ""
    @objc dynamic var desc: String = ""
    @objc dynamic var duration: Int = 0
    @objc dynamic var categoryId: Int = 0
    @objc dynamic var categoryName: String = ""
    @objc dynamic var notice: Bool = false
    @objc dynamic var forceNotice: Bool = false
    @objc dynamic var programmUrlString: String = ""
    
    static func findAll(by categoryId: Int) -> Results<ProgramGuide>? {
        return Realm.current?.objects(self).filter("categoryId = \(categoryId)")
    }
    
    override static func indexedProperties() -> [String] {
        return ["programmUrlString"]
    }
    
    
}
