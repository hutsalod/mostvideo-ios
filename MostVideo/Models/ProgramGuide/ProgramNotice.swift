//
//  ProgramNotice.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift

final class ProgramNotice: Object {
    
    @objc dynamic var programId: Int = 0
    @objc dynamic var isActive: Bool = false

    func validated() -> Self? {
        guard !isInvalidated else {
            return nil
        }
        return self
    }
    
}
