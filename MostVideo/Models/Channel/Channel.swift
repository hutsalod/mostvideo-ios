//
//  Channel.swift
//  MostVideo
//
//  Created by Roman on 15.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

/// <#Description#>
final class Channel: BaseModel {
    @objc dynamic var name: String?
    @objc dynamic var code: String?
    @objc dynamic var imageHolder: String?
    @objc dynamic var streamUrl: String?
    @objc dynamic var streamHDUrl: String?
    @objc dynamic var programmUrl: String?
}
