//
//  BaseModel.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/8/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class BaseModel: Object {
    
    @objc dynamic var id: Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func find(withIdentifier identifier: Int) -> Self? {
        return Realm.current?.objects(self).filter("id = \(identifier)").first
    }
    
    func validated() -> Self? {
        guard !isInvalidated else {
            return nil
        }
        return self
    }
}

