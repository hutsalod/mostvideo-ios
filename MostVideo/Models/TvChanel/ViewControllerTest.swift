//
//  ViewControllerTest.swift
//  MostVideo
//
//  Created by Mac Book  on 06.07.2020.
//  Copyright © 2020 MostVideo. All rights reserved.
//

import UIKit

class ViewControllerTest:  UIViewController, UITableViewDelegate, UITableViewDataSource {


// Data model: These strings will be the data for the table view cells
let animals: [String] = ["База пуста"]
    
    
    @IBOutlet weak var tebleView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tebleView.delegate = self
      tebleView.dataSource = self
        tebleView.separatorStyle = .none
    self.tebleView.reloadData()
    }
    
    func tableView(_ tebleView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.animals.count
    }
    
    // create a cell for each table view row
    func tableView(_ tebleView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
       let cell = tebleView.dequeueReusableCell(withIdentifier: "UserCell") as! UITableViewCell
               
        
        // set the text from the data model
        cell.textLabel?.text = self.animals[indexPath.row]
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tebleView(_ tebleView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }


}


