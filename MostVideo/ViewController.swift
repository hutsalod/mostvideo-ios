//
//  ViewController.swift
//  MostVideo
//
//  Created by Roman on 09.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit

import FacebookCore
import FacebookLogin
import FBSDKLoginKit

class ViewController: UIViewController {
    
    var dict: [String : AnyObject]!
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let myLoginButton = UIButton(type: .custom)
        myLoginButton.backgroundColor = UIColor.darkGray
        myLoginButton.frame = CGRect(x: 0, y: 0, width: 180, height: 40)

        myLoginButton.center = view.center;
        myLoginButton.setTitle("My Login Button", for: .normal)
        
        // Handle clicks on the button
        myLoginButton.addTarget(self, action: #selector(self.loginButtonClicked), for: .touchUpInside)
        
        // Add the button to the view
        view.addSubview(myLoginButton)
        
        if let _ = AccessToken.current {
            getFBUserData()
        }
        
        textField.text = UserDefaults.standard.object(forKey: PushManager.kDeviceTokenKey) as? String
    }
    
    @IBAction func Logout(_ sender: UIButton) {
        LoginManager().logOut()
    }
    
    // Once the button is clicked, show the login dialog
    @objc func loginButtonClicked() {
        let loginManager = LoginManager()
        
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(_, _, let token):
             //   print("Logged in! authenticationToken = \(token.authenticationToken)")
                self.getFBUserData()
            }
        }
    }
    
    func getFBUserData(){
        guard let _ = AccessToken.current else {
            return
        }
        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start { (connection, result, error) in
            if (error == nil){
                self.dict = result as? [String : AnyObject]
            }
        }
    }
}

