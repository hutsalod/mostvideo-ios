//
//  AuthViewController.swift
//  MostVideo
//
//  Created by Roman on 14.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import RealmSwift
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import AuthenticationServices

class AuthViewController: UIViewController, ASAuthorizationControllerPresentationContextProviding {
    

    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        // return the current view window
        
        
        return self.view.window!
    }
    
    
    
    @IBOutlet weak var appleSign: UIStackView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginFbButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    
    private var isRequesting = false
    var dict = [String : AnyObject]()
    
    var em = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        em = UserDefaults.standard.string(forKey: "email") ?? ""
        print("++++")
          print(em)
      
             // Always adopt a light interface style.
             overrideUserInterfaceStyle = .light
       
//        if #available(iOS 11.0, *) {
//            self.navigationController?.navigationBar.prefersLargeTitles = true
//        }

        // Do any additional setup after loading the view.
        let siwaButton = ASAuthorizationAppleIDButton(type: .default, style: .white)
    // set this so the button will use auto layout constraint
        siwaButton.translatesAutoresizingMaskIntoConstraints = false

        // add the button to the view controller root view

        // the function that will be executed when user tap the button
        siwaButton.addTarget(self, action: #selector(appleSignInTapped), for: .touchUpInside)
         appleSign.addArrangedSubview(siwaButton)
           
    }
    
  @objc func appleSignInTapped() {

        let provider = ASAuthorizationAppleIDProvider()

        let request = provider.createRequest()
        // request full name and email from the user's Apple ID
        request.requestedScopes = [.fullName, .email]

        // pass the request to the initializer of the controller
        let authController = ASAuthorizationController(authorizationRequests: [request])
      
        // similar to delegate, this will ask the view controller
        // which window to present the ASAuthorizationController
        authController.presentationContextProvider = self
      
        // delegate functions will be called when user data is
        // successfully retrieved or error occured
        authController.delegate = self
        
        // show the Sign-in with Apple dialog
        authController.performRequests()
  
    }
    
    @IBAction func loginButtonAction(_ sender: UIButton) {
        guard !isRequesting else {
            return
        }
        
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        startLoading()
        AuthManager.auth(email, password: password, completion: self.handleAuthenticationResponse)
    }
    
    @IBAction func loginFbButtonAction(_ sender: UIButton) {
        guard !isRequesting else {
            return
        }

        let loginManager = LoginManager()
        
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: self) { [weak self] loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(_, _, let token):
              //  print("Logged in! authenticationToken = \(token.authenticationToken)")
                
                GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start { [weak self] (connection, result, error) in
                    if (error == nil) {
                        if let result = result as? [String : AnyObject],
                            let email = result["email"] as? String, let id = result["id"] as? String,
                            let handler = self?.handleAuthenticationResponse
                        {
                            SwiftNotice.wait()
                            var name = ""
                            var lastName = ""
                            if let fullName = result["name"] as? String {
                                var components = fullName.components(separatedBy: " ")
                                if(components.count > 0)
                                {
                                    name = components.removeFirst()
                                    lastName = components.joined(separator: " ")
                                }
                            }
                            AuthManager.auth(email, password: "", facebookId: id, name: name, lastName: lastName, completion: handler)
                            
                        }
                    }
                }
            }
        }
        
        
        
        
    }
    
    func startLoading() {
        // loginButton.startLoading()
        isRequesting = true
    }
    
    func stopLoading() {
        // loginButton.stopLoading()
        isRequesting = false
    }
    
    internal func handleAuthenticationResponse(_ response: LoginResponse) {
        SwiftNotice.clear()
        switch response {
        case .resource:
            break
        case .error(let apiError):
            switch apiError.code {
            case 1:
                Alert(key: "error.data_incorrect").present()
            case 2:
                Alert(key: "error.user_not_found").present()
            case 3:
                Alert(key: "error.user_not_active").present()
            case 4:
                Alert(key: "error.password_incorrect").present()
            case 5:
                Alert(key: "error.fb_email_empty").present()
            case 6:
                Alert(key: "error.user_deleted").present()
            default:
                Alert(key: "error.login").present()
            }
            stopLoading()
            return
        }
        
        stopLoading()
        dismiss(animated: true, completion: nil)
        AppManager.reloadApp()
    }
}
extension AuthViewController : ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("authorization error")
        guard let error = error as? ASAuthorizationError else {
            return
        }

        switch error.code {
        case .canceled:
            // user press "cancel" during the login prompt
            print("Canceled")
        case .unknown:
            // user didn't login their Apple ID on the device
            print("Unknown")
        case .invalidResponse:
            // invalid response received from the login
            print("Invalid Respone")
        case .notHandled:
            // authorization request not handled, maybe internet failure during login
            print("Not handled")
        case .failed:
            // authorization failed
            print("Failed")
        @unknown default:
            print("Default")
        }
    }
    

    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // unique ID for each user, this uniqueID will always be returned
            let userID = appleIDCredential.user ?? ""
            
            print(userID)

            // optional, might be nil
            var email = appleIDCredential.email ?? ""

            // optional, might be nil
            let givenName = appleIDCredential.fullName?.givenName ?? ""

            // optional, might be nil
            let familyName = appleIDCredential.fullName?.familyName ?? ""

            // optional, might be nil
            let nickName = appleIDCredential.fullName?.nickname ?? ""

            /*
                useful for server side, the app can send identityToken and authorizationCode
                to the server for verification purpose
            */
            var identityToken : String!
            if let token = appleIDCredential.identityToken {
                identityToken = String(bytes: token, encoding: .utf8)
            }

            var authorizationCode : String!
            if let code = appleIDCredential.authorizationCode {
                authorizationCode = String(bytes: code, encoding: .utf8)
            }
            let handler = self.handleAuthenticationResponse
            

            print(authorizationCode!+"@gmail.com")
            
            print("=======")
            print(email)
            if(!email.isEmpty){
                print("save")
            UserDefaults.standard.set(email, forKey: "email")
            }else{
                email = em
            }
            
            
           
          print(email)
            
             print("=======")
            AuthManager.auth(userID.sha1()+"@gmail.com", password: "", facebookId: userID.sha1(), name: givenName, lastName: familyName, completion: handler)
          // do what you want with the data here
        }
    }
}

extension ViewController : ASAuthorizationControllerPresentationContextProviding {

    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        // return the current view window
        return self.view.window!
    }
}
