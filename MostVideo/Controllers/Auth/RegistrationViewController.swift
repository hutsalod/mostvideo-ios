//
//  RegistrationViewController.swift
//  MostVideo
//
//  Created by Roman on 20.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import Alamofire

class RegistrationViewController: UITableViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordConfirmTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var sendButton: UIButton!
    
    private var isRequesting = false
    
    private func bindHeaderFooter() {
        tableView.register(UINib(nibName: CustomHeader.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: CustomHeader.identifier)
    }
    
    func startLoading() {
        // loginButton.startLoading()
        isRequesting = true
    }
    
    func stopLoading() {
        // loginButton.stopLoading()
        isRequesting = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindHeaderFooter()
        tableView.backgroundColor = Globals.Colors.backgroundTableColor
        tableView.tableFooterView = UIView()
    }
    
    @IBAction func enterButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendButtonAction(_ sender: UIButton) {
        guard !isRequesting else {
            return
        }
        
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        let passwordConfirm = passwordConfirmTextField.text ?? ""
        let name = nameTextField.text ?? ""
        let lastname = lastnameTextField.text ?? ""
        let phone = phoneTextField.text ?? ""
        
        guard name.count > 0 else {
            Alert(key: "error.name_is_empty").present()
            return
        }
        
        guard lastname.count > 0 else {
            Alert(key: "error.lastname_is_empty").present()
            return
        }
        
        guard email.count > 0 else {
            Alert(key: "error.email_is_empty").present()
            return
        }
        
        
        guard phone.count > 0 else {
            Alert(key: "error.phone_is_empty").present()
            return
        }

        guard password.count >= 6 else {
            Alert(key: "error.passwords_length").present()
            return
        }
        
        guard password == passwordConfirm else {
            Alert(key: "error.passwords_missmatch").present()
            return
        }
        
        startLoading()
        
        let parameters = [
            "USER_LOGIN": email.trimmingCharacters(in: .whitespaces),
            "USER_PASSWORD": password,
            "USER_CONFIRM_PASSWORD": password,
            "NAME": name.trimmingCharacters(in: .whitespaces),
            "LAST_NAME": lastname.trimmingCharacters(in: .whitespaces),
            "PERSONAL_PHONE": phone,
            "DEVID": UIDevice.current.identifierForVendor?.uuidString ?? ""
        ]
        
        guard let url = URL(string: Globals.Server.host) else {
            return
        }
        
        RegistrationManager.register(url: url, parameters: parameters) { [weak self] result in
            self?.stopLoading()
            if result {
                Alert(key: "global.confirm_email").present(handler: { [weak self] _ in
                    self?.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = nil
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = nil
        return headerView
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 4
        case 1:
            return 2
        case 2:
            return 1
        default:
            return 0
        }
    }
}
