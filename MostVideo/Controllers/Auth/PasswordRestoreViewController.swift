//
//  PasswordRestoreViewController.swift
//  MostVideo
//
//  Created by Roman on 17.09.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import WebKit

class PasswordRestoreViewController: UIViewController, WKUIDelegate {

    var webView: WKWebView!
    
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = URL(string: Globals.URL.passwordRestore)  {
            webView.load(URLRequest(url: url))
        } else {
            Alert(key: "error.open_password_restore").present()
        }
    }
    
    @IBAction func closeButtonAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

}
