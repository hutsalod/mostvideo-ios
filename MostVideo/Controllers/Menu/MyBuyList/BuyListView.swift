//
//  BuyListView.swift
//  MostVideo
//
//  Created by Mac Book  on 06.07.2020.
//  Copyright © 2020 MostVideo. All rights reserved.
//

import UIKit

class BuyListView: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var dateLastLoginLabel: UILabel!
    
    @IBOutlet var category: UILabel!
    @IBOutlet var balance: UILabel!
    
    

    
    func setUsers(user: BuyListAdapter) {
        balance.layer.masksToBounds = true
        balance.layer.cornerRadius = 15.0
        balance.text = user.bay
        nameLabel.text = user.title
        category.text = user.category
        dateLastLoginLabel.text = user.data
    }
    
}
