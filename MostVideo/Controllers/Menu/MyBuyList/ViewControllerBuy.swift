//
//  ViewControllerBuyList.swift
//  MostVideo
//
//  Created by Mac Book  on 06.07.2020.
//  Copyright © 2020 MostVideo. All rights reserved.
//

import UIKit

class ViewControllerBuy:  UIViewController,UITableViewDataSource, UITableViewDelegate, ViewControllerBuys {
    

    @IBOutlet var tableView: UITableView!
    
    var videos: [BuyAdapter] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
          tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none

        self.videos.append(BuyAdapter(data: "Стронгмен", title: "Чемпионат Украины по стронгмену. 1й Этап..." , category: "Сезон", bay: " 100₴  "))
        self.videos.append(BuyAdapter(data: "Стронгмен", title: "Кубок Украины по стронгмену 2019" , category: "Сезон", bay: " 100₴  "))
        self.videos.append(BuyAdapter(data: "Американский футбол", title: "STALLIONS - PATRIOTS финал чемпионата Укр..." , category: "Сезон", bay: " 100₴  "))
        self.videos.append(BuyAdapter(data: "Стронгмен", title: "Чемпионат Украины по стронгмену. 1й Этап..." , category: "Сезон", bay: " 100₴  "))
     
        tableView.reloadData()
    }
    
 

    func showBuy(){
        print("tests")
        let refreshAlert = UIAlertController(title: "Видео", message: "Видео успешно куплено!", preferredStyle: UIAlertControllerStyle.alert)

        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
        }))

        present(refreshAlert, animated: true, completion: nil)
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let video = videos[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! BuyView
        cell.delegate = self
        cell.setUsers(user: video)
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     //   self.performSegue(withIdentifier: "ArchiveDetail", sender: self)
    }
}

protocol ViewControllerBuys {
    
     func showBuy()
 }
