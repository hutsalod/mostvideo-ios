//
//  ViewControllerBuyList.swift
//  MostVideo
//
//  Created by Mac Book  on 06.07.2020.
//  Copyright © 2020 MostVideo. All rights reserved.
//

import UIKit
import RealmSwift


class ViewControllerBuyList:  UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    
    var videos: [BuyListAdapter] = []
    
    @IBOutlet var labelpadding: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
          tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none

            //   self.videos.append(BuyListAdapter(data: "Стронгмен", title: "Чемпионат Украины по стронгмену. 1й Этап..." , category: "Сезон", bay: " 100₴  "))
        BuyView()
        tableView.reloadData()
    }




    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let video = videos[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! BuyListView
        
        cell.setUsers(user: video)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     //   self.performSegue(withIdentifier: "ArchiveDetail", sender: self)
    }
    
    
    func BuyView(){
      
           let auth = AuthManager.isAuthenticated(realm:  Realm.current)
           let url = NSURL(string: "https://mostvideo.tv/rest/videoarchive/get_video_pay_history/")
           let request = NSMutableURLRequest(url: url! as URL)
           request.setValue("KEY:" + (auth?.token)!, forHTTPHeaderField: "Authorization-Token") //**
           request.httpMethod = "POST"
           let session = URLSession.shared

           let mData = session.dataTask(with: request as URLRequest) { data, response, error in
           if let data = data {
               do {
                           if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                            print(json)
                         if let result = json["result"] as? [String: AnyObject] {
                           if let any = result["DATA"] as? [[String: AnyObject]]{
                                                   
                                                    for result in any {
                                         
                                           let NAME =  result["NAME"] as! String
                                           let PAY_TYPE =  result["PAY_TYPE"] as! String
                                           let COST =  result["COST"] as! String
                                                        let PROMO =  result["PROMO"] as! String
                                                             self.videos.append(BuyListAdapter(data: PROMO, title: NAME, category: PAY_TYPE, bay: COST))
                           }
                           }
                           }
                            DispatchQueue.main.async {
                                self.tableView.reloadData()

                                  }
                           }

                      } catch let error {
                          print(error)
                      }
        
           }
           }
           mData.resume()

           
        }
}

