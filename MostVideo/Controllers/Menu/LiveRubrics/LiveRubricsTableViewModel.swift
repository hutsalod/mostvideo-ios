//
//  LiveRubricsTableViewModel.swift
//  MostVideo
//
//  Created by Roman on 20.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//


import RealmSwift

final class LiveRubricsTableViewModel {
    
    var rubrics: List<Rubric>? {
        return Realm.current?.objects(UserProfile.self).first?.liveRubrics
      //  return Realm.current?.objects(Rubric.self)
    }
    
}

extension LiveRubricsTableViewModel {
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return rubrics?.count ?? 0
    }
    
    func rubricForRowAt(row: Int) -> Rubric? {
        return rubrics?[row]
    }
    
}
