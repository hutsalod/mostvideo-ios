//
//  LiveRubricsTableViewController.swift
//  MostVideo
//
//  Created by Roman on 20.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import RealmSwift

class LiveRubricsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var switchButton: UIButton! {
        didSet {
            switchButton.setTitle("Включить все", for: .normal)
            switchButton.setTitle("Отключить все", for: .selected)
            switchButton.layer.cornerRadius = 3
            switchButton.layer.borderWidth = 1
            switchButton.layer.borderColor = Globals.Colors.tintColor.cgColor
        }
    }
    
    var viewModel = LiveRubricsTableViewModel()
    
    private func bindCell() {
        tableView.register(UINib(nibName: LiveRubricCell.identifier, bundle: nil), forCellReuseIdentifier: LiveRubricCell.identifier)
    }
    
    private func bindHeaderFooter() {
        tableView.register(UINib(nibName: CustomHeader.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: CustomHeader.identifier)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bindCell()
        bindHeaderFooter()
        tableView.backgroundColor = Globals.Colors.backgroundTableColor
        tableView.tableFooterView = UIView()
        updateLiveRubrics()
    }
    
    private func updateLiveRubrics() {
        SwiftNotice.wait()
        if let allRubrics = Realm.current?.objects(UserProfile.self).first?.allRubrics {
            switchButton.isSelected = allRubrics
        }
        
        UserProfileManager.getUserProfile { [weak self] getUserProfileResult in
            SwiftNotice.clear()
            
            if let allRubrics = Realm.current?.objects(UserProfile.self).first?.allRubrics {
                self?.switchButton.isSelected = allRubrics
            }
            
            self?.reloadData()
        }
    }
    
    private func reloadData() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    @IBAction func switchButtonAction(_ sender: UIButton) {
        SwiftNotice.wait()
        let buttonState = switchButton.isSelected
        UserProfileManager.setAllRubrics(isSelected: !switchButton.isSelected) { [weak self] setRubricsResult in
            UserProfileManager.getUserProfile { [weak self] getUserProfileResult in
                SwiftNotice.clear()
                self?.reloadData()
                self?.switchButton.isSelected = !buttonState
                NotificationCenter.default.post(name: Notification.Name("UpdateProgramGuide"), object: nil, userInfo: nil)
            }
        }
    }
    
    // MARK: - Table view data source
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = "Уведомлять о LIVE трансляциях"
        return headerView
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
//            return nil
//        }
//        headerView.titleLabel.text = nil
//        return headerView
//    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LiveRubricCell.identifier) as? LiveRubricCell else {
            return UITableViewCell()
        }
        cell.reset()
        if let rubric = viewModel.rubricForRowAt(row: indexPath.row) {
            cell.rubric = rubric
        }
        return cell
    }
    
}
