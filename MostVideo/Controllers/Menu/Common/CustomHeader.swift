//
//  CustomHeader.swift
//  MostVideo
//
//  Created by Roman on 13.09.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit

class CustomHeader: UITableViewHeaderFooterView {
    static let identifier = "CustomHeader"

    @IBOutlet weak var titleLabel: UILabel!
    

}
