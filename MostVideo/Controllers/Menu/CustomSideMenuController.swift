//
//  CustomSideMenuController.swift
//  MostVideo
//
//  Created by Roman on 15.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit

class CustomSideMenuController: SideMenuController {
    
    required init?(coder aDecoder: NSCoder) {
        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "Menu")
        SideMenuController.preferences.drawing.sidePanelPosition = .overCenterPanelLeft
        SideMenuController.preferences.drawing.sidePanelWidth = UIDevice.current.userInterfaceIdiom == .pad ? 500 : min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.animating.statusBarBehaviour = .showUnderlay 
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        performSegue(withIdentifier: "embedInitialCenterController", sender: nil)
        performSegue(withIdentifier: "embedSideController", sender: nil)
    }
    
}

