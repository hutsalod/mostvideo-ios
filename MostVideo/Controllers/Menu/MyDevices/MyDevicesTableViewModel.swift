//
//  MyDevicesTableViewModel.swift
//  MostVideo
//
//  Created by Roman on 21.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation

import Foundation
import RealmSwift

final class MyDevicesTableViewModel {
    
    var devices: Results<Device>? {
        //return Realm.current?.objects(Device.self)
        return Realm.current?.objects(UserProfile.self).first?.devices.sorted(byKeyPath: "date", ascending: false)
    }
}

extension MyDevicesTableViewModel {
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return devices?.count ?? 0
    }
    
    func deviceForRowAt(row: Int) -> Device? {
        return devices?[row]
    }
    
}
