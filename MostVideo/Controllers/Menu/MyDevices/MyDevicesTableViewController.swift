//
//  MyDevicesTableViewController.swift
//  MostVideo
//
//  Created by Roman on 21.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit

class MyDevicesTableViewController: UITableViewController {
    
    var viewModel = MyDevicesTableViewModel()
    
    private func bindCell() {
        tableView.register(UINib(nibName: MyDeviceCell.identifier, bundle: nil), forCellReuseIdentifier: MyDeviceCell.identifier)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bindCell()
        updateDiviceList()
        tableView.tableFooterView = UIView()

       
    }
    
    private func updateDiviceList() {
        SwiftNotice.wait()
        UserProfileManager.getUserProfile { [weak self] getUserProfileResult in
            SwiftNotice.clear()
            self?.reloadData()
        }
    }

    private func reloadData() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
   
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MyDeviceCell.identifier) as? MyDeviceCell else {
            return UITableViewCell()
        }
        cell.reset()
        if let device = viewModel.deviceForRowAt(row: indexPath.row) {
            cell.device = device
        }
        return cell
    }

}
