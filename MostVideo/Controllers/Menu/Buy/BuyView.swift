//
//  BuyListView.swift
//  MostVideo
//
//  Created by Mac Book  on 06.07.2020.
//  Copyright © 2020 MostVideo. All rights reserved.
//

import UIKit

class BuyView: UITableViewCell{

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var dateLastLoginLabel: UILabel!
    
    @IBOutlet var category: UILabel!
    @IBOutlet var balance: UILabel!
    
    @IBOutlet var BuyButton: UIButton!
    var delegate:ViewControllerBuys?
    @IBAction func clikBuy(_ sender: Any) {
        print("tests2")
        self.delegate?.showBuy()
    }
    
    
    func setUsers(user: BuyAdapter) {
        balance.layer.masksToBounds = true
        balance.layer.cornerRadius = 8.0
        balance.text = user.bay
        nameLabel.text = user.title
        category.text = user.category
        dateLastLoginLabel.text = user.data
    }
    
}




