//
//  CurrentSubscriptionsTableViewController.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit

class CurrentSubscriptionsTableViewController: UITableViewController {

    var viewModel = CurrentSubscriptionsTableViewModel()
    
    private func bindCell() {
        tableView.register(UINib(nibName: CurrentSubscriptionCell.identifier, bundle: nil), forCellReuseIdentifier: CurrentSubscriptionCell.identifier)
    }
    
    private func bindHeaderFooter() {
        tableView.register(UINib(nibName: CustomHeader.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: CustomHeader.identifier)
    }
    
    
    private func updateCurrentSubscriptions() {
        SwiftNotice.wait()
        SubscriptionManager.getSubscriptionsList { [weak self] (getSubscriptionsListResult) in
            UserProfileManager.getUserProfile { [weak self] getUserProfileResult in
                SwiftNotice.clear()
                self?.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindCell()
        bindHeaderFooter()
        tableView.backgroundColor = Globals.Colors.backgroundTableColor
        tableView.tableFooterView = UIView()
        updateCurrentSubscriptions()
    }
    
    private func reloadData() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = nil
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = nil
        return headerView
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CurrentSubscriptionCell.identifier) as? CurrentSubscriptionCell else {
            return UITableViewCell()
        }
        cell.reset()
        if let currentSubscription = viewModel.currentSubscriptionForRowAt(indexPath: indexPath) {
            cell.currentSubscription =  currentSubscription
        }
        return cell
    }

}
