//
//  CurrentSubscriptionsTableViewModel.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//


import RealmSwift

final class CurrentSubscriptionsTableViewModel {
    
    var currentSubscription: List<CurrentSubscription>? {
       // return Realm.current?.objects(CurrentSubscription.self)
       return Realm.current?.objects(UserProfile.self).first?.subscriptions
    }

}

extension CurrentSubscriptionsTableViewModel {
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return currentSubscription?.count ?? 0
    }
    
    func currentSubscriptionForRowAt(indexPath: IndexPath) -> CurrentSubscription? {
        return currentSubscription?[indexPath.row]
    }

}
