//
//  EditProfileTableViewController.swift
//  MostVideo
//
//  Created by Roman on 22.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import RealmSwift

class EditProfileTableViewController: UITableViewController {
    

    @IBOutlet weak var readyBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var emailLabel: UILabel!
    
    var userProfile: UserProfile? {
        didSet {
            guard userProfile?.validated() != nil else { return }
            updateProfileInformation()
        }
    }
    
    @IBOutlet weak var nameTextField: UITextField! {
        didSet {
            nameTextField.delegate = self
        }
    }
    
    @IBOutlet weak var lastnameTextField: UITextField! {
        didSet {
            lastnameTextField.delegate = self
        }
    }
    @IBOutlet weak var phoneTextField: UITextField! {
        didSet {
            phoneTextField.delegate = self
        }
    }
    
    private func bindHeaderFooter() {
        tableView.register(UINib(nibName: CustomHeader.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: CustomHeader.identifier)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateProfile(isHidden: false)
        readyBarButtonItem.isEnabled = false
        tableView.tableFooterView = UIView()
        bindHeaderFooter()
        tableView.backgroundColor = Globals.Colors.backgroundTableColor
        if let userProfile = Realm.current?.objects(UserProfile.self).first {
            self.userProfile = userProfile
        }

    }
    
    private func updateProfile(isHidden: Bool) {
        if !isHidden { SwiftNotice.wait() }
        UserProfileManager.getUserProfile { [weak self] getUserProfileResult in
            if let userProfile = Realm.current?.objects(UserProfile.self).first {
                self?.userProfile = userProfile
                SwiftNotice.clear()
            }
        }
    }
    
    private func updateProfileInformation() {
        guard let userProfile = self.userProfile else { return }
        
        nameTextField.text = userProfile.name
        lastnameTextField.text = userProfile.lastName
        phoneTextField.text = userProfile.phone
        emailLabel.text = userProfile.email
    }
    
    @IBAction func closeBarButtonAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func readyBarButtonAction(_ sender: UIBarButtonItem) {
        view.endEditing(true)
        UserProfileManager.setNameAndPhone(name: nameTextField.text, lastname: lastnameTextField.text, phone: phoneTextField.text) { [weak self] result in
            if result {
                SwiftNotice.showNoticeWithText(.success, text: "OK", autoClear: true, autoClearTime: 1)
                self?.readyBarButtonItem.isEnabled = false
                self?.updateProfile(isHidden: true)
                NotificationCenter.default.post(name: Notification.Name("UpdateProfile"), object: nil, userInfo: nil)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = nil
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = nil
        return headerView
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
}

extension EditProfileTableViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        readyBarButtonItem.isEnabled = true
        return true
    }
    
}
