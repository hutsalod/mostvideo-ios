//
//  EditProfileTableViewController.swift
//  MostVideo
//
//  Created by Roman on 22.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import RealmSwift
import DatePickerDialog

class FilterTableViewController: UITableViewController  {

    
    
   var delegate:ViewControllerFiltr!
    @IBOutlet weak var readyBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet var picket: UIPickerView!
    @IBOutlet var textSeason: UIButton!
    @IBOutlet var yearText: UIButton!
    @IBOutlet var startText: UIButton!
    @IBOutlet var endText: UIButton!
    
    let pickerData = [
             ["value": "2014", "display": "2014"],
             ["value": "2015", "display": "2015"],
             ["value": "2016", "display": "2016"],
             ["value": "2017", "display": "2017"],
              ["value": "2018", "display": "2018"],
              ["value": "2019", "display": "2019"],
              ["value": "2020", "display": "2020"],
        ]
    var pickerSeason = [["value": "13051", "display": "Баскетбол"],
                        ["value": "13052", "display": "Хоккей"],
        ["value": "13053", "display": "Мотоспорт"],
        ["value": "13054", "display": "Американский футбол"],
        ["value": "13055", "display": "Футзал"],
        ["value": "13056", "display": "Гандбол"],
        ["value": "13057", "display": "Конференция"],
        ["value": "25375", "display": "Борьба"],
        ["value": "31270", "display": "Атлетика"],
        ["value": "31271", "display": "Теннис"],
        ["value": "31272", "display": "Волейбол"],
        ["value": "31274", "display": "Фитнес"],
        ["value": "31276", "display": "Авто-Мото премьера"],
        ["value": "39819", "display": "MMA"],
        ["value": "39820", "display": "Сквош"],
        ["value": "44714", "display": "Конный спорт "],
        ["value": "54206", "display": "Футбол"],
        ["value": "94782", "display": "Премьера"],
        ["value": "184111", "display": "CryptoMost"],
        ["value": "189541", "display": "MostMoto"],
        ["value": "691484", "display": "Каратэ"],
        ["value": "693455", "display": "Most Media Promotion"],
        ["value": "708743", "display": "Стронгмен"],
        ["value": "831784", "display": "Грэпплинг"],
        ["value": "695682", "display": "Стронгмен"],
        ["value": "858431", "display": "Конференция SMC"],
        ["value": "922536", "display": "#sport_на_карантині"],
    ]
    
    var strData: String?
    var index: Int = 0
    var userProfile: UserProfile? {
        didSet {
            guard userProfile?.validated() != nil else { return }
      }
    }
    
    @IBOutlet weak var nameTextField: UITextField! {
        didSet {
            nameTextField.delegate = self
        }
    }
    

    @IBAction func clikCategory(_ sender: Any) {
        self.picket.isHidden = false
        self.picket.layoutIfNeeded()
    }
    
    @IBAction func startSeson(_ sender: Any) {

    }
    
    @IBOutlet weak var lastnameTextField: UITextField! {
        didSet {
            lastnameTextField.delegate = self
        }
    }
    @IBOutlet weak var phoneTextField: UITextField! {
        didSet {
            phoneTextField.delegate = self
        }
    }
    
    @IBAction func startFilter(_ sender: Any) {
        DatePickerDialog().show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
             (date) -> Void in
             if let dt = date {
                         let formatter = DateFormatter()
                         formatter.dateFormat = "MM.dd.yyyy"
                         self.startText.setTitle(formatter.string(from: dt), for: .normal)
                     }
         }
    }
    
    @IBAction func endFilter(_ sender: Any) {
        
        DatePickerDialog().show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
             (date) -> Void in
             if let dt = date {
                         let formatter = DateFormatter()
                         formatter.dateFormat = "MM.dd.yyyy"
                self.endText.setTitle(formatter.string(from: dt), for: .normal)
                     }
         }

    }
    

    
    
    @IBAction func filtrSearch(_ sender: Any) {
        self.delegate.VideoViews(category: strData ?? "")
       dismiss(animated: true, completion: nil)
        
    
    }
    @IBAction func seasonClik(_ sender: Any) {
        
        PickerDialog().show(title: "Сезон", options: pickerSeason, selected: "season") {
                     (value, display) -> Void in
                     print("Unit selected: \(value). display: \(display)")
                    self.strData = value
                  self.textSeason.titleLabel?.text = display
                 }
    }
    
    @IBAction func yearClik(_ sender: Any) {
        
        PickerDialog().show(title: "Год", options: pickerData, selected: "year") {
                     (value, display) -> Void in
                     print("Unit selected: \(value). display: \(display)")
                  self.yearText.titleLabel?.text = display
                 }
    }
    
    
    private func bindHeaderFooter() {
        tableView.register(UINib(nibName: CustomHeader.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: CustomHeader.identifier)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
          AdapterView()
 

        tableView.backgroundColor = Globals.Colors.backgroundTableColor
  
      
        
   
 
    
    }
    
    private func updateProfile(isHidden: Bool) {
        if !isHidden { SwiftNotice.wait() }
        UserProfileManager.getUserProfile { [weak self] getUserProfileResult in
            if let userProfile = Realm.current?.objects(UserProfile.self).first {
                self?.userProfile = userProfile
                SwiftNotice.clear()
            }
        }
    }
    

    
    @IBAction func closeBarButtonAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func readyBarButtonAction(_ sender: UIBarButtonItem) {
        view.endEditing(true)
   
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = nil
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = nil
        return headerView
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
     func AdapterView(){
    
         let auth = AuthManager.isAuthenticated(realm:  Realm.current)
         let url = NSURL(string: "https://mostvideo.tv/rest/videoarchive/get_video_category/")
         let request = NSMutableURLRequest(url: url! as URL)
         request.setValue("KEY:" + (auth?.token)!, forHTTPHeaderField: "Authorization-Token") //**
         request.httpMethod = "POST"
    

         URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
         if let data = data {
             do {
                         if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                       if let result = json["result"] as? [String: AnyObject] {
                         if let any = result["DATA"] as? [[String: AnyObject]]{
                            self.pickerSeason.removeAll()
                                         for result in any {
                                         let NAME =  result["NAME"] as! String
                                            let ID =  result["ID"] as! String
                                               DispatchQueue.main.async() {
                                            self.pickerSeason.append(contentsOf: [["value":ID,"display":NAME]])
                                            }
                         }}}
                           
                }
                
                    } catch let error {
                        print(error)
                    }
      
         }
         }.resume()
         
      }
}

protocol ViewControllerFiltr {

    func VideoViews(category: String)
}

extension FilterTableViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        readyBarButtonItem.isEnabled = true
        return true
    }
    
}
