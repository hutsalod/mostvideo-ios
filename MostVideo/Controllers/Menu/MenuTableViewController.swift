//
//  MenuTableViewController.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import RealmSwift

class MenuTableViewController: UITableViewController {
    
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    let titles = ["Контент", "Платежи", "Настройки", "Профиль", ""]
    
    var userProfile: UserProfile? {
        didSet {
            guard userProfile?.validated() != nil else { return }
            updateProfileInformation()
        }
    }
    
  
    @IBAction func buttonOut(_ sender: Any) {
        //#selector(SideMenuController.toggle)
        self.sideMenuController?.toggle()
    }
    
    private func updateProfile(isHidden: Bool) {
        if !isHidden { SwiftNotice.wait() }
        IAPManager.shared.sendValidSubscriptionToServer(productIds: productIds)
        UserProfileManager.getUserProfile { [weak self] getUserProfileResult in
            if let userProfile = Realm.current?.objects(UserProfile.self).first {
                self?.userProfile = userProfile
                SwiftNotice.clear()
            }
        }
        
        
    }
    
    private func updateProfileInformation() {
        guard let userProfile = self.userProfile else { return }
        let fullName = "\(userProfile.name ?? "") \(userProfile.lastName ?? "")".trimmingCharacters(in: .whitespaces)
        fullnameLabel?.text = fullName.count > 0 ? fullName : "ФИО"
        phoneLabel?.text = userProfile.phone
        emailLabel?.text = userProfile.email
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = titles[section]
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = nil
        return headerView
    }
    
    private func bindHeaderFooter() {
        tableView.register(UINib(nibName: CustomHeader.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: CustomHeader.identifier)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
        } 

        guard let sideMenuController = self.sideMenuController else {
            return
        }
        navigationItem.leftBarButtonItems = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.stop, target: sideMenuController, action: #selector(SideMenuController.toggle))]
        tableView.tableFooterView = UIView()
        bindHeaderFooter()
        tableView.backgroundColor = Globals.Colors.backgroundTableColor
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateProfileSelector), name: Notification.Name("UpdateProfile"), object: nil)
        updateProfile(isHidden: true)
        
        
    }
    
    @objc private func updateProfileSelector() {
        IAPManager.shared.sendValidSubscriptionToServer(productIds: productIds)
        UserProfileManager.getUserProfile { [weak self] getUserProfileResult in
            if let userProfile = Realm.current?.objects(UserProfile.self).first {
                self?.userProfile = userProfile
            }
        }
    }
    
    func menuDidShown() {
        print("menuDidShown")
        updateProfile(isHidden: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 5
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 3
        case 2:
            return 3
        case 3:
            return 1
        case 4:
            return 1
        default:
            return 0
        }
    }
    
    @IBAction func logoutButtonAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name("Logout"), object: nil, userInfo: nil)
        AuthManager.logout {
            AppManager.reloadApp()
        }
    }
    
}
