//
//  WorkoutTableViewController.swift
//  MostVideo
//
//  Created by Roman on 23.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import RealmSwift

class WorkoutTableViewController: UITableViewController {
    let tagOffset = 100
    var viewModel = WorkoutTableViewModel()
    var allWorkouts = [[Workout]]()
    
    @IBOutlet weak var totalWorkoutsSwitch: UISwitch!
    @IBOutlet weak var fiveMinutesSwitch: UISwitch!
    
    @IBOutlet var timeTextFields: [UITextField]!
    
    @IBOutlet var workoutsLabels: [UILabel]!
    
    
    var myPickerView: UIPickerView!

    
    private func bindHeaderFooter() {
        tableView.register(UINib(nibName: CustomHeader.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: CustomHeader.identifier)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timeTextFields.forEach { $0.delegate = self }
        tableView.tableFooterView = UIView()
        bindHeaderFooter()
        tableView.backgroundColor = Globals.Colors.backgroundTableColor
        updateWorkouts()
    }
    
    private func updateWorkouts() {
        SwiftNotice.wait()
        allWorkouts = viewModel.getAllWorkouts()
        updateUI()
        UserProfileManager.getUserProfile { [weak self] getUserProfileResult in
            SwiftNotice.clear()
            if let allWorkouts = self?.viewModel.getAllWorkouts() {
               self?.allWorkouts = allWorkouts
            }
            self?.updateUI()
            self?.reloadData()
        }
    }

    private func reloadData() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    private func updateUI() {

        if let userProfile = Realm.current?.objects(UserProfile.self).first {
            let morningWorkoutsActive = userProfile.morningWorkoutsActive
            totalWorkoutsSwitch.isOn = morningWorkoutsActive
            fiveMinutesSwitch.isOn = userProfile.fiveMin
            
            for day in 1...7 {
                if let timeTextField = self.view.viewWithTag(day + tagOffset) as? UITextField {
                    if let hour = viewModel.getActiveWorkout(for: day)?.hour {
                        timeTextField.text = "\(hour):05"
                    } else {
                         timeTextField.text = "не напоминать"
                    }
                }
            }
            
            fiveMinutesSwitch.isEnabled = morningWorkoutsActive
            workoutsLabels.forEach { $0.isEnabled = morningWorkoutsActive }
            timeTextFields.forEach { $0.isEnabled = morningWorkoutsActive }
            timeTextFields.forEach { $0.textColor = morningWorkoutsActive ? UIColor.black : UIColor.gray}
        }
    }
    
    @IBAction func totalWorkoutsSwitchChanged(_ sender: UISwitch) {
        SwiftNotice.wait()
        UserProfileManager.setAllWorkouts(isActive: sender.isOn) { [weak self] _ in
            UserProfileManager.getUserProfile { [weak self] _ in
                SwiftNotice.clear()
                self?.updateUI()
                self?.reloadData()
            }
        }
    }
    
    @IBAction func fiveMinutesSwitchChanged(_ sender: UISwitch) {
        SwiftNotice.wait()
        UserProfileManager.setFiveMin(isActive: sender.isOn) { [weak self] _ in
            UserProfileManager.getUserProfile { [weak self] _ in
                SwiftNotice.clear()
                self?.updateUI()
                self?.reloadData()
            }
        }
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let timeTextField = self.view.viewWithTag(indexPath.row - 1 + tagOffset) as? UITextField {
            timeTextField.becomeFirstResponder()
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = "Расписание"
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = nil
        return headerView
    }
    
    
    @objc func doneClick(_ sender: UIBarButtonItem) {
        view.endEditing(true)
        let day = sender.tag - 3 * tagOffset
        let row = self.myPickerView.selectedRow(inComponent: 0)
        
        let id = (row == 0) ? viewModel.getAllWorkouts(for: day)[row].id : viewModel.getAllWorkouts(for: day)[row - 1].id
        print("doneClick day=\(day) \(id)")
        SwiftNotice.wait()
        UserProfileManager.setWorkout(id: id, isActive: row != 0) { [weak self] _ in
            UserProfileManager.getUserProfile { [weak self] _ in
                SwiftNotice.clear()
                self?.updateUI()
                self?.reloadData()
            }
        }
        
        
    }
    
    func pickUp(_ textField : UITextField){

        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.tag = textField.tag + tagOffset  // 201, 202 ...
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        textField.inputView = self.myPickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = .black
        toolBar.sizeToFit()

        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(doneClick))
        doneButton.tag = textField.tag + 2 * tagOffset  // 301, 302, ...
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }

}

extension WorkoutTableViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let day = pickerView.tag - 2 * tagOffset
        return viewModel.getAllWorkouts(for: day).count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    //   self.textField.text = pickerData[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let day = pickerView.tag - 2 * tagOffset
        if row == 0 {
            return "не напоминать"
        } else {
            return "\(viewModel.getAllWorkouts(for: day)[row - 1].hour):05"
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUp(textField)
    }
    
    
}
