//
//  WorkoutTableViewModel.swift
//  MostVideo
//
//  Created by Roman on 11.09.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift

final class WorkoutTableViewModel {
    
    var workouts: List<Workout>? {
     //   return Realm.current?.objects(Workout.self)
        return Realm.current?.objects(UserProfile.self).first?.morningWorkouts
    }
}

extension WorkoutTableViewModel {
    
    func getActiveWorkout(for day: Int) -> Workout? {
        guard let workouts = workouts else {
            return nil
        }
        for workout in workouts {
            if workout.day == day && workout.checked {
                return workout
            }
        }
        return nil
    }
    
    func getAllWorkouts(for day: Int) -> [Workout] {
        var result = [Workout]()
        
        guard let workouts = workouts else {
            return result
        }
        
        for workout in workouts {
            if workout.day == day {
                result.append(workout)
            }
        }
        return result.sorted(by: { $0.hour < $1.hour })
    }
    
    func getAllWorkouts() -> [[Workout]] {
        var result = [[Workout]]()

        for day in 1...7 {
            result.append(getAllWorkouts(for: day))
        }
        return result
    }
    
}
