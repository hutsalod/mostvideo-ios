//
//  BuySubscriptionViewController.swift
//  MostVideo
//
//  Created by Roman on 21.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import WebKit

class BuySubscriptionViewController: UIViewController, WKUIDelegate {

    var webView: WKWebView!
    var urlString: String?
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let urlString = urlString, let url = URL(string: urlString)  {
            webView.load(URLRequest(url: url))
        } else {
            Alert(key: "error.open_buy_subscription").present()
        }
    }
    
    @IBAction func closeButtonAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
}



