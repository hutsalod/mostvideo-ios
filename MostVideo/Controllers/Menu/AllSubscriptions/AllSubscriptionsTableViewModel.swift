//
//  AllSubscriptionsTableViewModel.swift
//  MostVideo
//
//  Created by Roman on 21.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyStoreKit

struct IAPSubscription {
    var identifier: String
    var localizedDescription: String
    var localizedPrice: String
}



final class AllSubscriptionsTableViewModel {
    
//    var subscriptions: Results<Subscription>? {
//        return Realm.current?.objects(Subscription.self)
//    }
    
    var subscriptions = [IAPSubscription]()
}

extension AllSubscriptionsTableViewModel {
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return subscriptions.count
    }
    
    func subscriptionForRowAt(row: Int) -> IAPSubscription {
        return subscriptions[row]
    }
    
    func identifierForRowAt(row: Int) -> String {
        return subscriptions[row].identifier
    }
    

    func retrieveProductsInfo(completionHandler: @escaping () -> Void) {
        SwiftyStoreKit.retrieveProductsInfo(productIds) { [weak self] result in
            self?.subscriptions.removeAll()
            for retrievedProduct in result.retrievedProducts {
                if let localizedPrice = retrievedProduct.localizedPrice {
                    self?.subscriptions.append(IAPSubscription(identifier: retrievedProduct.productIdentifier, localizedDescription: retrievedProduct.localizedTitle, localizedPrice: localizedPrice))
                }
            }
            completionHandler()
        }
    }
    
//    func buyingSubscriptIsLongerThanCurrent(index: Int) -> Bool {
//        if let currentSubscription = Realm.current?.objects(UserProfile.self).first?.subscriptions.first,
//            let buyingSubscription = subscriptions?[index],
//            let dateTo = currentSubscription.dateTo {
//            let durationInSec: Double =  Double(buyingSubscription.duration) * 24 * 60 * 60
//            if Date().addingTimeInterval(durationInSec).timeIntervalSince1970 > dateTo.timeIntervalSince1970 {
//                return true
//            } else {
//                return false
//            }
//        } else {
//            return true
//        }
//    }
    
}
