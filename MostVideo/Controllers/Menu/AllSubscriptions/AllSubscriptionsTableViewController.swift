//
//  AllSubscriptionsTableViewController.swift
//  MostVideo
//
//  Created by Roman on 21.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import SwiftyStoreKit

class AllSubscriptionsTableViewController: UITableViewController {
    
    var viewModel = AllSubscriptionsTableViewModel()
    var selectedUrl: String?
    
    private func bindCell() {
        tableView.register(UINib(nibName: AllSibscriptionsCell.identifier, bundle: nil), forCellReuseIdentifier: AllSibscriptionsCell.identifier)
    }
    
    private func bindHeaderFooter() {
        tableView.register(UINib(nibName: CustomHeader.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: CustomHeader.identifier)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bindCell()
        updateAllSubscriptions()
        tableView.tableFooterView = UIView()
        bindHeaderFooter()
        tableView.backgroundColor = Globals.Colors.backgroundTableColor
    }
    
    
    private func updateAllSubscriptions() {
        SwiftNotice.wait()
        
        viewModel.retrieveProductsInfo { [weak self] in
            SwiftNotice.clear()
            self?.reloadData()
        }
//        SubscriptionManager.getSubscriptionsList { [weak self]  result in
//            SwiftNotice.clear()
//            self?.reloadData()
//        }
    }
    
    private func reloadData() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
//    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return "Варианты подписки"
//    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.text = "Варианты подписки"
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeader.identifier) as? CustomHeader else {
            return nil
        }
        headerView.titleLabel.font = UIFont.systemFont(ofSize: 12)
        headerView.titleLabel.text = "Доступ к каналам Most 2 и 3"
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AllSibscriptionsCell.identifier) as? AllSibscriptionsCell else {
            return UITableViewCell()
        }
        cell.reset()
        cell.subscription = viewModel.subscriptionForRowAt(row: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        SwiftyStoreKit.purchaseProduct(viewModel.identifierForRowAt(row: indexPath.row)) { result in
            switch result {
            case .success(purchase: let product):
                IAPManager.shared.sendValidSubscriptionToServer(productIds: productIds)
                print("Purchase Success: \(product.productId)")
            case .error(let error):
                Alert(title: localized("global.error"), message: error.localizedDescription).present()
            }
        }
        
//        if viewModel.buyingSubscriptIsLongerThanCurrent(index: indexPath.row) {
//            selectedUrl = viewModel.subscriptionUrlForRowAt(row: indexPath.row)
//            performSegue(withIdentifier: "ShowBuySegue", sender: nil)
//        } else {
//            Alert(key: "error.current_subscription_is_longer").present()
//        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowBuySegue",
            let nc = segue.destination as? UINavigationController,
            let vc = nc.viewControllers.first as? BuySubscriptionViewController {
            vc.urlString = selectedUrl
        }
        
    }

}
