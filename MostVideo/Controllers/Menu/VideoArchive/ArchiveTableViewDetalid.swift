//
//  ArchiveTableViewDetalid.swift
//  MostVideo
//
//  Created by Mac Book  on 06.07.2020.
//  Copyright © 2020 MostVideo. All rights reserved.
//

import Kingfisher
import UIKit
import RealmSwift

class ArchiveTableViewDetalid: UIViewController,UITableViewDataSource, UITableViewDelegate, BMPlayerDelegate {
    
    @IBOutlet var loadStatus: UIActivityIndicatorView!
    

    @IBOutlet weak var titleLabel: UILabel!
    
        @IBOutlet var player: BMCustomPlayer!
        @IBOutlet var tableView: UITableView!

        var strData: String?
        var urlData: String?
        var videos: [ArchiveAdapter] = []
    
        var dataTask: URLSessionDataTask?
        

        override func viewDidLoad() {
            super.viewDidLoad()
            titleLabel.text = strData
              tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
                  VideoView()
            setupPlayer(urlVideo: urlData ?? "")
            
        }


    
     func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        player.pause()
        let video = videos[indexPath.row]
        self.strData = video.title
        self.titleLabel.text = video.title
        self.setupPlayer(urlVideo: video.url ?? "")
    }
    

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
           return videos.count
       
        }
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell = tableView.dequeueReusableCell(withIdentifier: "ArchiveCell") as! ArchiveCell
        cell.setUsers(user: videos[indexPath.row])
                return cell
          
        }
        
  
        
    private func setupPlayer(urlVideo: String) {
        
        let escapedString = urlVideo.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            player.delegate = self
            BMPlayerConf.shouldAutoPlay = true
            player.backBlock = { (isFullScreen) in
                return
            }
         
        let url = URL(string: escapedString!)!

         let asset = BMPlayerResource(url: url,
                                      name: strData ??  "")
            player.setVideo(resource: asset)
    
 
        }
    

      func VideoView(){
        dataTask?.cancel()
          let auth = AuthManager.isAuthenticated(realm:  Realm.current)
          let url = URL(string: "https://mostvideo.tv/rest/videoarchive/get_video_catalog/")
          let request = NSMutableURLRequest(url: url! as URL)
          request.setValue("KEY:" + (auth?.token)!, forHTTPHeaderField: "Authorization-Token") //**
          request.httpMethod = "POST"
          let session = URLSession(configuration: .default)

          // Create a task that will be responsible for downloading the image
        dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
      
              guard error == nil else { return }
              if let httpResponse = response as? HTTPURLResponse {
               
                  if let datas = data {
                   do {
                                             if let json = try JSONSerialization.jsonObject(with: datas) as? [String: Any] {
                                                        DispatchQueue.main.async() {
                                                                self.loadStatus.isHidden = true
                                                                    }
                                                 print("4 \(json)" )
                                           if let result = json["result"] as? [String: AnyObject] {
                                                 print("5")
                                             if let any = result["DATA"] as? [[String: AnyObject]]{
                                               
                                                             for result in any {
                                                             let NAME =  result["NAME"] as! String
                                                             let CATEGORY =  result["CATEGORY"] as! String
                                                                 let CATEGORY_ID =  result["CATEGORY_ID"] as! String
                                                             let PREVIEW_PIC =  result["PREVIEW_PIC"] as! String
                                                                 let URL_VIDEO =  result["URL"] as! String
                                                                 print(NAME)
                                                             var COST =  result["COST"] as! String
                                                                         if COST.count <= 1 {
                                                                            COST = " Бесплатно "
                                                                         }
                                                                 let url =  URL(string: PREVIEW_PIC)!
                                                                 let data = try? Data(contentsOf: url)
                                                                 
                                                                  KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                                                                                                                                                                          self.videos.append(ArchiveAdapter(image: CATEGORY, title: NAME , abode: COST,imageVideo: image ?? UIImage(named:"preview")!, url: URL_VIDEO))
                                                                                                                                                               })
                                                                                                                   DispatchQueue.main.async() {
                                                                                            self.tableView.reloadData()
                                                                                                                }
                                                              
                                             }}}}
                                        } catch let error {
                                            print(error)
                                        }
                  }
              }
          }

        self.dataTask?.resume()
          
       }
    
    override func viewWillDisappear(_ animated: Bool) {

       dataTask?.cancel()
        
    }
    
 
    func bmPlayer(player: BMPlayer, playerStateDidChange state: BMPlayerState) {
       }
       
       func bmPlayer(player: BMPlayer, loadedTimeDidChange loadedDuration: TimeInterval, totalDuration: TimeInterval) {
       }
       
       func bmPlayer(player: BMPlayer, playTimeDidChange currentTime: TimeInterval, totalTime: TimeInterval) {
       }
       
       func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
       }
       
       func bmPlayer(player: BMPlayer, playerOrientChanged isFullscreen: Bool) {
           print("full: \(isFullscreen)")
           UIApplication.shared.setStatusBarHidden(isFullscreen, with: .fade)
           self.navigationController?.setNavigationBarHidden(isFullscreen, animated: false)
       }
    
}

