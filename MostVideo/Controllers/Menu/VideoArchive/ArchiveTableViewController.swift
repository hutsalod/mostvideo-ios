//
//  ArchiveTableViewController.swift
//  MostVideo
//
//  Created by Mac Book  on 02.07.2020.
//  Copyright © 2020 MostVideo. All rights reserved.
//

import Kingfisher

import UIKit
import RealmSwift
class ArchiveTableViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    
        
    @IBOutlet var loadStatus: UIActivityIndicatorView!
    
    @IBOutlet var tableView: UITableView!
    
    var dataTask: URLSessionDataTask?

    
     var videos: [ArchiveAdapter] = []
   
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.stop, target: self, action: #selector(backAction))
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.reloadData()
        
        VideoView(category: "")
    }


    @IBAction func chooseButtonTapped(_ sender: UIButton) {
           let selectionVC = storyboard?.instantiateViewController(withIdentifier: "FilterTableViewController") as! FilterTableViewController
           selectionVC.delegate = self
           present(selectionVC, animated: true, completion: nil)
       }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let selectedString = tableView.indexPathForSelectedRow?.row ?? 0
        (segue.destination as? ArchiveTableViewDetalid)?.strData = videos[selectedString].title
        (segue.destination as? ArchiveTableViewDetalid)?.urlData = videos[selectedString].url
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let video = videos[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArchiveCell") as! ArchiveCell
        
        cell.setUsers(user: video)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     //   self.performSegue(withIdentifier: "ArchiveDetail", sender: self)
    }
    

    
    func VideoView(category: String){

        let auth = AuthManager.isAuthenticated(realm:  Realm.current)
        let url = NSURL(string: "https://mostvideo.tv/rest/videoarchive/get_video_catalog/")
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("KEY:" + (auth?.token)!, forHTTPHeaderField: "Authorization-Token") //**
        request.httpMethod = "POST"
        let session = URLSession.shared

        let task = session.dataTask(with: request as URLRequest) { data, response, error in
        if let data = data {
            do {
                        if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                            DispatchQueue.main.async() {
                        self.loadStatus.isHidden = true
                            }
                      if let result = json["result"] as? [String: AnyObject] {
                           
                        if let any = result["DATA"] as? [[String: AnyObject]]{
                                        for result in any {
                                        let NAME =  result["NAME"] as! String
                                        let CATEGORY =  result["CATEGORY"] as! String
                                            let CATEGORY_ID =  result["CATEGORY_ID"] as! String
                                        let PREVIEW_PIC =  result["PREVIEW_PIC"] as! String
                                            let URL_VIDEO =  result["URL"] as! String
                                        var COST =  result["COST"] as! String
                                                    if COST.count <= 1 {
                                                       COST = " Бесплатно "
                                                    }
                                            let url =  URL(string: PREVIEW_PIC)!
                                       
                                            
                                            
                                            if  category.count > 1{
                                           if  CATEGORY_ID.contains(category){
                                          KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                                                                                                                                                  self.videos.append(ArchiveAdapter(image: CATEGORY, title: NAME , abode: COST,imageVideo: image ?? UIImage(named:"preview")!, url: URL_VIDEO))
                                                                                                                                       })
                                                                                           DispatchQueue.main.async() {
                                                                                               
                                                                                       
                                                                                        
                                                                                        self.tableView.reloadData()
                                                                                        }
                                                }
                                            }else{
                                                KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                                                                                                       self.videos.append(ArchiveAdapter(image: CATEGORY, title: NAME , abode: COST,imageVideo: image ?? UIImage(named:"preview")!, url: URL_VIDEO))
                                                                                            })
                                                DispatchQueue.main.async() {
                                                    
                                            
                                             
                                             self.tableView.reloadData()
                                             }
                                            }
                        }}}}
                   } catch let error {
                       print(error)
                   }
        }
        }
        task.resume()
        
     }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    @objc func backAction(){
        //print("Back Button Clicked")
        dismiss(animated: true, completion: nil)
    }
    

    

}

extension ArchiveTableViewController: ViewControllerFiltr{

    func VideoViews(category: String) {
        self.VideoView(category: category)
    }
    
}
