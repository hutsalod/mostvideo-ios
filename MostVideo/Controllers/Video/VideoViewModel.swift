//
//  VideoViewModel.swift
//  MostVideo
//
//  Created by Roman on 16.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import RealmSwift

final class VideoViewModel {
    var programGuide: Results<ProgramGuide>? {
        return Realm.current?.objects(ProgramGuide.self)
    }
    
    var groupedItems = [Date:Results<ProgramGuide>]()
    var itemDates = [Date]()
}

extension VideoViewModel {
    
    func rebuild(for programUrl: String?) {
        
        groupedItems.removeAll()
        itemDates.removeAll()
        
        guard let programUrl = programUrl, let items = programGuide?.filter("programmUrlString = '\(programUrl)'"), items.count > 0 else {
            return
        }

        //Find each unique day for which an Item exists in your Realm
        itemDates = items.reduce(into: [Date](), { results, currentItem in
            let date = currentItem.date!
            let beginningOfDay = Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: date), month: Calendar.current.component(.month, from: date), day: Calendar.current.component(.day, from: date), hour: 0, minute: 0, second: 0))!
            let endOfDay = Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: date), month: Calendar.current.component(.month, from: date), day: Calendar.current.component(.day, from: date), hour: 23, minute: 59, second: 59))!
            //Only add the date if it doesn't exist in the array yet
            if !results.contains(where: { addedDate->Bool in
                return addedDate >= beginningOfDay && addedDate <= endOfDay
            }) {
                results.append(beginningOfDay)
            }
        })
        //Filter each Item in realm based on their date property and assign the results to the dictionary
        groupedItems = itemDates.reduce(into: [Date:Results<ProgramGuide>](), { results, date in
            let beginningOfDay = Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: date), month: Calendar.current.component(.month, from: date), day: Calendar.current.component(.day, from: date), hour: 0, minute: 0, second: 0))!
            let endOfDay = Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: date), month: Calendar.current.component(.month, from: date), day: Calendar.current.component(.day, from: date), hour: 23, minute: 59, second: 59))!
            results[beginningOfDay] = Realm.current?.objects(ProgramGuide.self).filter("date >= %@ AND date <= %@ AND programmUrlString = '\(programUrl)'", beginningOfDay, endOfDay).sorted(byKeyPath: "date", ascending: true)
        })
        itemDates.sort()
    }
    
    func numberOfInSections() -> Int {
        return itemDates.count
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return groupedItems[itemDates[section]]?.count ?? 0
    }
    
    func currentItem() -> IndexPath? {
        for (section, date) in itemDates.enumerated() {
            if let itemsOfDay = groupedItems[date] {
                for (row, item) in itemsOfDay.enumerated() {
                    if let itemDate = item.date, itemDate > Date() {
                        return IndexPath(row: row, section: section)
                    }
                }
            }
        }
        return nil
    }
    
    func programGuideForRowAt(indexPath: IndexPath) -> ProgramGuide? {
        return groupedItems[itemDates[indexPath.section]]?[indexPath.row]
    }
    
    func titleForSection(_ section: Int) -> String? {
        return itemDates[section].getDateStringForProgramGuide()
    }
}
