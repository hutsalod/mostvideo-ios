//
//  VideoViewController.swift
//  MostVideo
//
//  Created by Roman on 15.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit
import RealmSwift


class VideoViewController: UIViewController {
    
    @IBOutlet weak var player: BMCustomPlayer!
    @IBOutlet weak var tableView: UITableView!
    
    var channels: Results<Channel>? {
        return Realm.current?.objects(Channel.self).sorted(byKeyPath: "id", ascending: true)
    }
    
    private var curentIndexSelected = 0
    
    @IBOutlet weak var channelsSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var apect169: NSLayoutConstraint!
    @IBOutlet weak var safeTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var topSuperConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var channelsHeightViewConstraint: NSLayoutConstraint!
    
    private let viewModel = VideoViewModel()
    
    private func updateChannelsSelector() {
        guard let channels = channels else {
            return
        }
        channelsSegmentedControl.removeAllSegments()
        for (i,channel) in channels.enumerated() {
            channelsSegmentedControl.insertSegment(withTitle: channel.code, at: i, animated: false)
        }
    }
    
    private func currentProgrammUrlString() -> String? {
        guard let channelsCount = channels?.count, channelsCount > curentIndexSelected else {
            return nil
        }
        return channels?[curentIndexSelected].programmUrl
    }
    
    private func setupPlayer(channel: Channel) {
        player.delegate = self
        BMPlayerConf.shouldAutoPlay = false
        player.backBlock = { (isFullScreen) in
            return
        }
        
        guard let streamUrl = channel.streamUrl else { return }
        
        var definitions = [BMPlayerResourceDefinition]()
        
        if let sdUrl = URL(string: streamUrl) {
            definitions.append(BMPlayerResourceDefinition(url: sdUrl, definition: "SD"))
        }
        if let streamHDUrl = channel.streamHDUrl, let hdUrl = URL(string: streamHDUrl) {
            definitions.append(BMPlayerResourceDefinition(url: hdUrl, definition: "HD"))
        }
        
        let asset = BMPlayerResource(name: channel.name ?? "",
                                     definitions: definitions,
                                     cover: URL(string: channel.imageHolder ?? ""))
        player.setVideo(resource: asset)
        
        if UserDefaults.standard.bool(forKey: "hdVideo"), definitions.count > 1 {
            player.setVideo(resource: asset, definitionIndex: 1)
        } else {
            player.setVideo(resource: asset)
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            // fullscreen
            channelsHeightViewConstraint.constant = 0
            apect169.isActive = false
            heightConstraint.constant = UIScreen.main.bounds.height
            heightConstraint.isActive = true
            safeTopConstraint.isActive = false
            topSuperConstraint.isActive = true
            
        } else {
            // portraite
            channelsHeightViewConstraint.constant = 44
            
            apect169.isActive = true
            heightConstraint.isActive = false
            safeTopConstraint.isActive = true
            topSuperConstraint.isActive = false
        }
    }
    
    private func bindCell() {
        tableView.register(UINib(nibName: ProgramGuideCell.identifier, bundle: nil), forCellReuseIdentifier: ProgramGuideCell.identifier)
        tableView.register(UINib(nibName: ProgramGuideCustomHeader.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: ProgramGuideCustomHeader.identifier)
    }
    
    private func updateChanelsList() {
        ChannelManager.getChannelsList { [weak self] result in
            self?.updateChannelsSelector()
            guard let firstChannel = self?.channels?.first else { return }
            self?.setupPlayer(channel: firstChannel)
            self?.channelsSegmentedControl.selectedSegmentIndex = 0
            self?.updateProgramGuide()
        }
    }
    
//    private func currentSubscriptionIsActive(handler: @escaping (Bool)->(Void)) {
//        SwiftNotice.wait()
//        UserProfileManager.getUserProfile { getUserProfileResult in
//            if let dateTo = Realm.current?.objects(UserProfile.self).first?.subscriptions.first?.dateTo {
//                if Date().timeIntervalSince1970 < dateTo.timeIntervalSince1970 {
//                    SwiftNotice.clear()
//                    handler(true)
//                } else {
//                    SwiftNotice.clear()
//                    handler(false)
//                }
//            } else {
//                SwiftNotice.clear()
//                handler(false)
//            }
//        }
//    }
    
    @IBAction func channelSelectorChanged(_ sender: UISegmentedControl) {
        player.pause()
        switch sender.selectedSegmentIndex {
        case 0, 1:
            guard let channels = channels, channels.count > sender.selectedSegmentIndex else { return }
            setupPlayer(channel: channels[sender.selectedSegmentIndex])
            player.play()
            curentIndexSelected = sender.selectedSegmentIndex
            reloadData()
            updateProgramGuide()
        case 2, 3:
            guard let channels = channels, channels.count > sender.selectedSegmentIndex else { return }
            SwiftNotice.wait()
            IAPManager.shared.verifyValidAllSubscriptions(productIds: productIds) { [weak self] result in
                SwiftNotice.clear()
                if result {
                    self?.setupPlayer(channel: channels[sender.selectedSegmentIndex])
                    self?.player.play()
                    self?.curentIndexSelected = sender.selectedSegmentIndex
                } else {
                    Alert(key: "error.need_subscription").present(handler: { [weak self] (_) in
                        self?.player.play()
                    })
                    sender.selectedSegmentIndex = self?.curentIndexSelected ?? 0
                }
                self?.reloadData()
                self?.updateProgramGuide()
            }
        default:
            break
        }
    }
    
    private func updateProgramGuide(withScroll: Bool = true) {
        if let urlString = currentProgrammUrlString(), let url = URL(string: urlString) {
            ProgramGuideManager.getProgramGuide(url) { getGuideResult in
                ProgramGuideManager.getProgramNotices(completion: { [weak self] getNoticesResult in
                    UserProfileManager.getUserProfile(completion: { [weak self] getUserProfileResult in
                        self?.reloadData(withScroll: withScroll)
                    })
                })
            }
        }
    }
    
    @objc private func updateProgramGuideWithOutScroll() {
        self.updateProgramGuide(withScroll: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindCell()
        
        let label = UILabel(frame: tableView.frame)
        label.textAlignment = .center
        label.textColor = .gray
        tableView.backgroundView = label
        tableView.tableFooterView = UIView()
        
        viewModel.rebuild(for: currentProgrammUrlString())
        updateChannelsSelector()
        updateChanelsList()
      //  updateProgramGuide()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.prepareToLogout), name: Notification.Name("Logout"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateProgramGuideWithOutScroll), name: Notification.Name("UpdateProgramGuide"), object: nil)
        
    }
    
    func updateIsEmptyArticle() {
        guard let label = tableView.backgroundView as? UILabel else { return }
        if viewModel.itemDates.count == 0 {
            label.text = localized("programm.list.empty")
        } else {
            label.text = ""
        }
    }
    
    @objc private func prepareToLogout() {
        player?.prepareToDealloc()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func reloadData(withScroll: Bool = true) {
        DispatchQueue.main.async { [weak self] in
            self?.viewModel.rebuild(for: self?.currentProgrammUrlString())
            self?.tableView.reloadData()
            self?.updateIsEmptyArticle()
            if let currentItem = self?.viewModel.currentItem() {
                if withScroll {
                    self?.tableView.scrollToRow(at: currentItem, at: .middle, animated: false)
                }
            }
        }
    }
}

extension VideoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProgramGuideCell.identifier) as? ProgramGuideCell else {
            return UITableViewCell()
        }
        cell.reset()
        if let programGuide = viewModel.programGuideForRowAt(indexPath: indexPath) {
            cell.programGuide =  programGuide
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfInSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: ProgramGuideCustomHeader.identifier) as? ProgramGuideCustomHeader
        headerView?.backgroundView?.backgroundColor = .red
        headerView?.dateLabel.text = viewModel.titleForSection(section)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28.0
    }
    
}

extension VideoViewController: BMPlayerDelegate {
    func bmPlayer(player: BMPlayer, playerStateDidChange state: BMPlayerState) {
        
    }
    
    func bmPlayer(player: BMPlayer, loadedTimeDidChange loadedDuration: TimeInterval, totalDuration: TimeInterval) {
        
    }
    
    func bmPlayer(player: BMPlayer, playTimeDidChange currentTime: TimeInterval, totalTime: TimeInterval) {
        
    }
    
    func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
        
    }
    
    func bmPlayer(player: BMPlayer, playerOrientChanged isFullscreen: Bool) {
        print("full: \(isFullscreen)")
        UIApplication.shared.setStatusBarHidden(isFullscreen, with: .fade)
        self.navigationController?.setNavigationBarHidden(isFullscreen, animated: false)
    }
    
    
}
