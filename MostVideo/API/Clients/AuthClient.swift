//
//  AuthClient.swift

import Foundation
import Realm
import RealmSwift
import Alamofire

struct AuthClient: APIClient {
    let api: APIFetcher

    func login(parameters: Parameters, completion: @escaping (APIResponse<LoginResource>) -> Void) {

        api.fetch(LoginRequest(parameters: parameters)) { response in
            switch response {
            case .resource(let resource):
                guard let _ = resource.success, let token = resource.token else {
                    return completion(.error(APIError(code: resource.error_code ?? 0, description: resource.error ?? "")))
                }
                let auth = Auth()
                auth.lastAccess = Date()
                auth.token = token

                Realm.executeOnMainThread({ (realm) in
                    // Delete all the Auth objects, since we don't
                    // support multiple-server per database
                    realm.delete(realm.objects(Auth.self))

                    // PushManager.updatePushToken()
                    realm.add(auth)
                })

                completion(response)
            case .error:
                completion(response)
            }
        }
    }
}
