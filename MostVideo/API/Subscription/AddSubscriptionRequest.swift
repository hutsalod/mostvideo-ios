//
//  AddSubscriptionRequest.swift
//  MostVideo
//
//  Created by Roman on 07.12.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Alamofire
import SwiftyJSON

final class AddSubscriptionRequest: APIRequest {
    
    typealias APIResourceType = AddSubscriptionResource
    let path = "/rest/subscriptions/add/"
    let method: HTTPMethod = .post
    let parameters: Parameters
    
    init(parameters: Parameters) {
        self.parameters = parameters
    }
    
}

final class AddSubscriptionResource: APIResource {
    var status: Bool? {
        return raw?["result"]["status"].bool
    }
    
    var errorMessage: String? {
        return raw?["result"]["error"].string
    }
}

