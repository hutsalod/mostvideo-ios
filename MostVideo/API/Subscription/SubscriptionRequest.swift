//
//  SubscriptionRequest.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

final class SubscriptionRequest: APIRequest {
    typealias APIResourceType = SubscriptionResource
    let path = "/rest/subscriptions/types/"
}

final class SubscriptionResource: APIResource {
    var status: Int? {
        return raw?["status"].int
    }
    
    var error: String? {
        return raw?["error"].string
    }
    
    var subscriptions: [Subscription]? {
        return raw?["result"]["DATA"].arrayValue.map {
            let subscription = Subscription()
            subscription.map($0, realm: nil)
            return subscription
        }
    }
}
