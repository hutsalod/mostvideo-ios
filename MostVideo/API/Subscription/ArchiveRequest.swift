//
//  SubscriptionRequest.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

final class ArchiveRequest: APIRequest {
    typealias APIResourceType = ArchiveResource
    let path = "/rest/videoarchive/get_video_catalog/"
}

final class ArchiveResource: APIResource {
    var status: Int? {
        return raw?["status"].int
    }
    
    var error: String? {
        return raw?["error"].string
    }
    
    var archivemodel: [ArchiveModel]? {
        return raw?["result"]["DATA"].arrayValue.map {
            let archivemodel = ArchiveModel()
            archivemodel.map($0, realm: nil)
            return archivemodel
        }
    }
}
