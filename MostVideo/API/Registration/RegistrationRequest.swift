//
//  RegistrationRequest.swift
//  MostVideo
//
//  Created by Roman on 20.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Alamofire
import SwiftyJSON

final class RegistrationRequest: APIRequest {
    typealias APIResourceType = RegistrationResource
    let path = "/rest_api/register/"
    let method: HTTPMethod = .post
    let parameters: Parameters
    
    init(parameters: Parameters) {
        self.parameters = parameters
    }
}

final class RegistrationResource: APIResource {
    var success: Bool? {
        return raw?["success"].bool
    }
    
    var errorMessage: String? {
        return raw?["error"].string
    }
    
}

