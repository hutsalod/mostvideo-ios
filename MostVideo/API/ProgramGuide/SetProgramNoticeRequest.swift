//
//  SetProgramNoticeRequest.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Alamofire
import SwiftyJSON

final class SetProgramNoticeRequest: APIRequest {
    typealias APIResourceType = SetProgramNoticesResource
    let path = "/rest/tvprogram/notice/"
    let method: HTTPMethod = .post
    let parameters: Parameters
    
    init(parameters: Parameters) {
        self.parameters = parameters
    }
    
}

final class SetProgramNoticesResource: APIResource {
    var status: Bool? {
        return raw?["result"]["status"].bool
    }
    
    var errorMessage: String? {
        return raw?["result"]["error"].string
    }
}
