//
//  GetProgramNoticesRequest.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation

import SwiftyJSON

final class GetProgramNoticesRequest: APIRequest {
    typealias APIResourceType = GetProgramNoticesResource
    let path = "/rest/tvprogram/notice/"
}

final class GetProgramNoticesResource: APIResource {
    var status: Int? {
        return raw?["status"].int
    }
    
    var programNotices: [ProgramNotice]? {
        return raw?["result"].arrayValue.map {
            let programNotice = ProgramNotice()
            programNotice.map($0, realm: nil)
            return programNotice
        }
    }
    
    var errorMessage: String? {
        return raw?["error"].string
    }
}
