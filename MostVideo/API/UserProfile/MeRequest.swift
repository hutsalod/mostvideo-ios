//
//  MeRequest.swift


import SwiftyJSON

final class MeRequest: APIRequest {
    typealias APIResourceType = MeResource
    let path = "/rest/user/profile/"
}

final class MeResource: APIResource {
    var status: Int? {
        return raw?["status"].int
    }
    
    var user: UserProfile? {
        guard let userRaw = raw?["result"] else { return nil }
        let user = UserProfile()
        user.map(userRaw, realm: nil)
        return user
    }

    var errorMessage: String? {
        return raw?["error"].string
    }
}
