//
//  ChangeProfileRequest.swift
//  MostVideo
//
//  Created by Roman on 20.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Alamofire
import SwiftyJSON

final class ChangeProfileRequest: APIRequest {
    
    typealias APIResourceType = ChangeProfileResource
    let path = "/rest/user/profile/"
    let method: HTTPMethod = .post
    let parameters: Parameters
    
    init(parameters: Parameters) {
        self.parameters = parameters
    }
    
}

final class ChangeProfileResource: APIResource {
    var status: Bool? {
        return raw?["result"]["status"].bool
    }
    
    var errorMessage: String? {
        return raw?["result"]["error"].string
    }
}
