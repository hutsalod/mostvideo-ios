//
//  APIRequest.swift
//  Rocket.Chat
//
//  Created by Matheus Cardoso on 9/18/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import Foundation
import Alamofire

protocol APIRequest {
    associatedtype APIResourceType: APIResource
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: Parameters { get }
}

extension APIRequest {
	var method: HTTPMethod {
        return .get
    }
    
    var parameters: Parameters {
        return [String : Any]()
    }
}
