//
//  LoginRequest.swift
//


import SwiftyJSON
import Alamofire

final class LoginRequest: APIRequest {
    typealias APIResourceType = LoginResource
    let method: HTTPMethod = .post
    let path = "/rest_api/auth/"
    let parameters: Parameters

    init(parameters: Parameters) {
        self.parameters = parameters
    }
}

final class LoginResource: APIResource {
    var success: Bool? {
        return raw?["success"].bool
    }
    
    var error: String? {
        return raw?["error"].string
    }

    var token: String? {
        return raw?["token"].string
    }
    
    var error_code: Int? {
        return raw?["error_code"].int
    }
}

typealias LoginResponse = APIResponse<LoginResource>
