//
//  ChannelRequest.swift
//  MostVideo
//
//  Created by Roman on 15.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

final class ChannelRequest: APIRequest {
    typealias APIResourceType = ChannelResource
    let path = "/rest/channels/list/"
}

final class ChannelResource: APIResource {
    var status: Int? {
        return raw?["status"].int
    }
    
    var error: String? {
        return raw?["error"].string
    }
    
    var channels: [Channel]? {
        return raw?["result"]["DATA"].arrayValue.map {
            let channel = Channel()
            channel.map($0, realm: nil)
            return channel
        }
    }
    
}
