//
//  APIError.swift
//  MostVideo
//
//  Created by Roman on 14.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation

struct APIError {
    let code: Int
    let description: String
}
