//
//  API.swift
//  Rocket.Chat
//
//  Created by Matheus Cardoso on 9/18/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

protocol APIFetcher {
    func fetch<R: APIRequest>(_ request: R, completion: ((APIResponse<R.APIResourceType>) -> Void)?)
}

final class API: APIFetcher {
    let host: URL
    var authToken: String?
    
    convenience init?(host: String) {
        guard let url = URL(string: host) else {
            return nil
        }

        self.init(host: url)
    }

    init(host: URL) {
        self.host = host
    }
    
    func fetch<R: APIRequest>(_ request: R, completion: ((APIResponse<R.APIResourceType>) -> Void)?) {
        let headers: HTTPHeaders = [
            "Authorization-Token": "KEY:\(authToken ?? "")",
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
        ]
        
        
        print("token \(authToken)" )
        let requestUrl = host.appendingPathComponent(request.path)
        
        Alamofire
            .request(requestUrl, method: request.method, parameters: request.parameters, encoding: URLEncoding.httpBody, headers: headers)
            .responseJSON { response in
                
                if response.response?.statusCode == 403 {
                    DispatchQueue.main.async {
                        WindowManager.open(.auth)
                    }
                    completion?(.error(APIError(code: 0, description: "403 Error")))
                    return
                }
                
                switch response.result {
                case .success(let data):
                    print(data)
                    if let json = JSON(rawValue: data) {
                        completion?(.resource(R.APIResourceType(raw: json)))
                    } else {
                        completion?(.error(APIError(code: 0, description: "JSON representation error")))
                    }
                case.failure(let error):
                    print(error.localizedDescription)
                    completion?(.error(APIError(code: 0, description: "Alamofire failure response")))
                }
        }
    }

}
