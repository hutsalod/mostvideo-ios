//
//  RealmExecute.swift
//  MostVideo
//
//  Created by Roman on 13.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift

extension Realm {
    static let writeQueue = DispatchQueue(label: "tv.mostvideo.realm.write", qos: .background)
    
    func execute(_ execution: @escaping (Realm) -> Void, completion: (() -> Void)? = nil) {
        var backgroundTaskId: UIBackgroundTaskIdentifier?
        
        backgroundTaskId = UIApplication.shared.beginBackgroundTask(withName: "tv.mostvideo.realm.background", expirationHandler: {
            backgroundTaskId = UIBackgroundTaskInvalid
        })
        
        if let backgroundTaskId = backgroundTaskId {
            let config = self.configuration
            
            Realm.writeQueue.async {
                if let realm = try? Realm(configuration: config) {
                    try? realm.write {
                        execution(realm)
                    }
                }
                
                if let completion = completion {
                    DispatchQueue.main.async {
                        completion()
                        
                    }
                }
                
                UIApplication.shared.endBackgroundTask(backgroundTaskId)
            }
        }
    }
    
    static func execute(_ execution: @escaping (Realm) -> Void, completion: (() -> Void)? = nil) {
        Realm.current?.execute(execution, completion: completion)
    }
    
    static func executeOnMainThread(_ execution: @escaping (Realm) -> Void) {
        guard let currentRealm = Realm.current else { return }
        try? currentRealm.write {
            execution(currentRealm)
        }
    }
    
    // MARK: Mutate
    
    // This method will add or update a Realm's object.
    static func delete(_ object: Object) {
        guard !object.isInvalidated else { return }
        
        self.execute({ realm in
            realm.delete(object)
        })
    }
    
    // This method will add or update a Realm's object.
    static func update(_ object: Object) {
        self.execute({ realm in
            realm.add(object, update: .all)
        })
    }
    
    // This method will add or update a list of some Realm's object.
    static func update<S: Sequence>(_ objects: S) where S.Iterator.Element: Object {
        self.execute({ realm in
            realm.add(objects, update: .all)
        })
    }
}
