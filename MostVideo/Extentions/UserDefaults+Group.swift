//
//  UserDefaults+Group.swift
//  MostVideo
//
//  Created by Roman on 13.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation

extension UserDefaults {
    static var group: UserDefaults {
        guard let defaults = UserDefaults(suiteName: AppGroup.identifier) else {
            fatalError("Could not initialize UserDefaults with suiteName \(AppGroup.identifier)")
        }
        
        return defaults
    }
}
