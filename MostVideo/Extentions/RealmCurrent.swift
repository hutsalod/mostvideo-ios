//
//  RealmCurrent.swift
//  MostVideo
//
//  Created by Roman on 13.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

var realmConfiguration: Realm.Configuration?

extension Realm {
    static var current: Realm? {
        if let configuration = realmConfiguration {
            return try? Realm(configuration: configuration)
        } else {
            guard let url = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: AppGroup.identifier)
                else { return nil }
            let configuration = Realm.Configuration(
                fileURL: url.appendingPathComponent("MostVideo.realm"),
                deleteRealmIfMigrationNeeded: true
            )
            realmConfiguration = configuration
            return try? Realm(configuration: configuration)
        }
    }
}
