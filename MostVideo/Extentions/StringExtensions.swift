//
//  StringExtensions.swift
//  MostVideo
//
//  Created by Roman on 21.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation


extension String {
    func sha1() -> String {
        let data = self.data(using: String.Encoding.utf8)!
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0, CC_LONG(data.count), &digest)
        }
        return Data(bytes: digest).base64EncodedString()
    }
}


