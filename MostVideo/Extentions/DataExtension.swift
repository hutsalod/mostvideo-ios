//
//  DataExtension.swift
//  MostVideo
//
//  Created by Roman on 10.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation


extension Data {
    var hexString: String {
        return map { String(format: "%02.2hhx", arguments: [$0]) }.joined()
    }
    
}
