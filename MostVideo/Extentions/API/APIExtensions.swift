//
//  APIExtensions.swift
//  Rocket.Chat
//
//  Created by Matheus Cardoso on 11/27/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import RealmSwift

extension API {
    static func current(realm: Realm? = Realm.current) -> API? {
        guard
            let auth = AuthManager.isAuthenticated(realm: realm),
            let host = URL(string: Globals.Server.host)
        else {
            return nil
        }

        let api = API(host: host)
        api.authToken = auth.token
        
        return api
    }

}
