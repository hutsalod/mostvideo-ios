//
//  Globals.swift
//  MostVideo
//
//  Created by Roman on 13.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit

struct Globals {
    
    struct Server {
        static let host = "https://mostvideo.tv"
    }
    
//    struct ProgramGuide {
//        static let url = "https://mostvideo.tv/list_last.xml"
//    }
    
    struct URL {
        static let passwordRestore = "https://mostvideo.tv/cabinet/profile/?forgot_password=yes"
    }
    
    struct Colors {
        
        static let tintColor =  #colorLiteral(red: 0.9254901961, green: 0.4039215686, blue: 0.1098039216, alpha: 1)
        static let backgroundTableColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        
    }

}
