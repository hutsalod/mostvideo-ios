//
//  RegistrationManager.swift
//  MostVideo
//
//  Created by Roman on 20.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import Alamofire

struct RegistrationManager {
    static func register(url: URL, parameters: Parameters, completion: @escaping (Bool) -> Void) {
        
        
        API(host: url).fetch(RegistrationRequest(parameters: parameters)) { response in
            switch response {
            case .resource(let resource):
                guard let success = resource.success, success else {
                    Alert(title: "Ошибка", message: resource.errorMessage ?? "Ошибка при регистрации").present()
                    completion(false)
                    return
                }
                completion(true)
            case .error:
                Alert(key: "error.registration").present()
                completion(false)
            }
        }
    }
}
