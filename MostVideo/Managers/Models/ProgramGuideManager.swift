//
//  ProgramGuideManager.swift
//  MostVideo
//
//  Created by Roman on 16.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import Alamofire
import SWXMLHash

struct ProgramGuideManager {

    static func getProgramGuide(_ url: URL, completion: @escaping (Bool) -> Void) {
        Alamofire.request(url, method: HTTPMethod.get)
            .response { response in
                if let data = response.data {
                    let xml = SWXMLHash.parse(data)
                    
                    let programGuideArray: [ProgramGuide] = xml["ArrayOfPeredacha"]["peredacha"].all.map({ elem in
                        let programGuide = ProgramGuide()
                        programGuide.deserialize(programmUrlSting: url.absoluteString, values: elem)
                        return programGuide
                    })
                    
                    Realm.executeOnMainThread({ (realm) in
                        let oldProgramGuide = realm.objects(ProgramGuide.self).filter("programmUrlString = '\(url.absoluteString)'")
                        realm.delete(oldProgramGuide)
                        realm.add(programGuideArray, update: .all)
                    })
                    completion(true)
                } else {
                    completion(false)
                }
        }
    }
    
    static func getProgramNotices(completion: @escaping (Bool) -> Void) {
        API.current()?.fetch(GetProgramNoticesRequest(), completion: { response in
            switch response {
            case .resource(let resource):
                
                guard let status = resource.status, status == 200 else {
                    Alert(key: "error.get_program_notice").present()
                    completion(false)
                    return
                }
                
                Realm.executeOnMainThread({ (realm) in
                    let programGuide = realm.objects(ProgramGuide.self)
                    programGuide.setValue(false, forKey: "notice")
                    
                    if let programNotices = resource.programNotices {
                        programNotices.forEach({ (notice) in
                            let programGuideItem = ProgramGuide.find(withIdentifier: notice.programId)
                            programGuideItem?.notice = notice.isActive
                        })
                    }
                    
//                    realm.delete(realm.objects(ProgramNotice.self))
//                    if let programNotices = resource.programNotices {
//                        realm.add(programNotices.compactMap({ $0 }), update: false)
//                    }
                })
                
                
                
                completion(true)
                
            case .error:
                Alert(key: "error.get_program_notice").present()
                completion(false)
            }
        })
    }
    
    static func setProgramNotice(programId: Int, isActice: Bool, completion: @escaping (Bool) -> Void) {
        let parameters = [
            "PID": "\(programId)",
            "TYPE": isActice ? "add" : "delete"
        ]
        API.current()?.fetch(SetProgramNoticeRequest(parameters: parameters), completion: { response in
            switch response {
            case .resource(let resource):
                guard let status = resource.status, status else {
                    Alert(key: "error.set_program_notice").present()
                    completion(false)
                    return
                }
                completion(true)
            case .error:
                Alert(key: "error.set_program_notice").present()
                completion(false)
            }
        })
    }
}
