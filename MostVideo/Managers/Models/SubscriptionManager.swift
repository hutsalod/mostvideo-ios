//
//  SubscriptionManager.swift
//  MostVideo
//
//  Created by Roman on 17.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import Alamofire

struct SubscriptionManager {
    
    static func getSubscriptionsList(completion: @escaping (Bool) -> Void) {
        API.current()?.fetch(SubscriptionRequest()) { response in
            switch response {
            case .resource(let resource):
                guard let status = resource.status, status == 200 else {
                    Alert(key: "error.get_subscriptions_list").present()
                    completion(false)
                    return
                }
                Realm.executeOnMainThread({ (realm) in
                    realm.delete(realm.objects(Subscription.self))
                    if let subscriptions = resource.subscriptions {
                        realm.add(subscriptions.compactMap({ $0 }), update: .all)
                    }
                })
                completion(true)
            case .error:
                Alert(key: "error.get_subscriptions_list").present()
                completion(false)
            }
        }
    }
    
    static func addSubscription(subTypeId: Int, originalTransactionId: String, dateFrom: Date, dateTo: Date, completion: @escaping (Bool) -> Void) {
        let df = DateFormatter()
        df.dateFormat = "dd.MM.yyyy HH:mm:ss"
        let parameters = [
            "SUBTYPEID": "\(subTypeId)",
            "DATE_ACTIVE_FROM": df.string(from: dateFrom),
            "DATE_ACTIVE_TO": df.string(from: dateTo),
            "PAYID": originalTransactionId
        ]
        API.current()?.fetch(AddSubscriptionRequest(parameters: parameters), completion: { response in
            switch response {
            case .resource(let resource):
                guard let status = resource.status, status else {
                    print("addSubscription ok")
                    completion(false)
                    return
                }
                completion(true)
            case .error:
                print("addSubscription error")
                completion(false)
            }
        })
    }
}
