//
//  UserProfileManager.swift
//  MostVideo
//
//  Created by Roman on 16.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import Alamofire

struct UserProfileManager {
    
    static func getUserProfile(completion: @escaping (Bool) -> Void) {
        API.current()?.fetch(MeRequest()) { response in
            switch response {
            case .resource(let resource):
                
                guard let status = resource.status, status == 200, let userProfile = resource.user  else {
                    Alert(key: "error.get_user_profile").present()
                    completion(false)
                    return
                }
                Realm.executeOnMainThread({ (realm) in
                    realm.add(userProfile, update: .all)
                })
                completion(true)
            case .error:
                Alert(key: "error.get_user_profile").present()
                completion(false)
                
            }
        }
        

    }
    
    static func testmos(completion: @escaping (Bool) -> Void){
        API.current()?.fetch(ArchiveRequest()) { response in
            switch response {
            case .resource(let resource):
                
                guard let status = resource.status, status == 200, let userProfile = resource.archivemodel  else {
                    Alert(key: "error.get_user_profile").present()
                    completion(false)
                    return
                }
                Realm.executeOnMainThread({ (realm) in
                    realm.add(userProfile, update: .all)
                })
                completion(true)
            case .error:
                Alert(key: "error.get_user_profile").present()
                completion(false)
                
            }
        }
    }
    
    static func setLiveRubric(id: Int, isSelected: Bool, completion: @escaping (Bool) -> Void) {
        let parameters = [
            "RUBRIC_ID": "\(id)",
            "RUBRIC_CHECKED": "\(isSelected ? 1 : 0)"
        ]
        setUserProfile(parameters: parameters, completion: completion)
    }
    
    static func setAllRubrics(isSelected: Bool, completion: @escaping (Bool) -> Void) {
        let parameters = [
            "ALL_RUBRICS": "\(isSelected ? 1 : 0)"
        ]
        setUserProfile(parameters: parameters, completion: completion)
    }
    
    static func setAllWorkouts(isActive: Bool, completion: @escaping (Bool) -> Void) {
        let parameters = [
            "MORNING_WORKOUT_ACTIVE": "\(isActive ? 1 : 0)"
        ]
        setUserProfile(parameters: parameters, completion: completion)
    }
    
    static func setFiveMin(isActive: Bool, completion: @escaping (Bool) -> Void) {
        let parameters = [
            "FIVEMIN": "\(isActive ? 1 : 0)"
        ]
        setUserProfile(parameters: parameters, completion: completion)
    }
    
    static func setWorkout(id: Int, isActive: Bool, completion: @escaping (Bool) -> Void) {
        let parameters = [
            "MW_ID": "\(id)",
            "MW_CHECKED": "\(isActive ? 1 : 0)"
        ]
        setUserProfile(parameters: parameters, completion: completion)
    }
    
    static func setNameAndPhone(name: String?, lastname: String?, phone: String?, completion: @escaping (Bool) -> Void) {
        let parameters = [
            "NAME": "\(name ?? "")",
            "LAST_NAME": "\(lastname ?? "")",
            "PERSONAL_PHONE": "\(phone ?? "")"
        ]
        setUserProfile(parameters: parameters, completion: completion)
    }
    
    
    static func setUserProfile(parameters: Parameters, completion: @escaping (Bool) -> Void) {
        API.current()?.fetch(ChangeProfileRequest(parameters: parameters)) { response in
            switch response {
            case .resource(let resource):
                guard let status = resource.status, status else {
                    Alert(key: "error.set_user_profile").present()
                    completion(false)
                    return
                }
                completion(true)
            case .error:
                Alert(key: "error.set_user_profile").present()
                completion(false)
            }
        }
    }
    
}



