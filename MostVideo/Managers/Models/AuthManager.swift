//
//  AuthManager.swift
//  MostVideo
//
//  Created by Roman on 13.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import Alamofire

struct AuthManager {
    
    /**
     - returns: Last auth object (sorted by lastAccess), if exists.
     */
    static func isAuthenticated(realm: Realm? = Realm.current) -> Auth? {
        guard let realm = realm else { return nil }
        return realm.objects(Auth.self).sorted(byKeyPath: "lastAccess", ascending: false).first
    }
    
    static func getDeviceIdentifier() -> String? {
        return UIDevice.current.identifierForVendor?.uuidString
    }
    
    static func getDeviceModelName() -> String {
        return UIDevice.modelName
    }

    /**
     This method authenticates the user with email and password.
     
     - parameter email: Email
     - parameter password: Password
     - facebookId: Facebook Id
     - parameter completion: The completion block that'll be called in case
     of success or error.
     */
    
    static func auth(_ email: String, password: String, facebookId: String? = nil, name: String = "", lastName: String = "", completion: @escaping (LoginResponse) -> Void) {
        var parameters: [String: Any]?
        
        let privateKey = "MostVideo_4app!"
        let devId = getDeviceIdentifier() ?? ""
        print("\(privateKey)\(devId)\(privateKey)".sha1())

        if let facebookId = facebookId {
            parameters = [
                "USER_LOGIN": facebookId,
                "USER_PASSWORD": email,
                "FB": "\(privateKey)\(devId)\(privateKey)".sha1(),
                "DEVID":  devId,
                "DEVTOKEN":  PushManager.getDeviceToken() ?? "",
                "DEVNAME": getDeviceModelName(),
                "USER_LAST_NAME": lastName.trimmingCharacters(in: .whitespaces),
                "USER_NAME": name.trimmingCharacters(in: .whitespaces)
            ]
        } else {
            parameters = [
                "USER_LOGIN": email,
                "USER_PASSWORD": password,
                "FB": "",
                "DEVID":  devId,
                "DEVTOKEN":  PushManager.getDeviceToken() ?? "",
                "DEVNAME": getDeviceModelName(),
                "USER_LAST_NAME": "",
                "USER_NAME": ""
            ]
        }
        
        if let parameters = parameters {
            self.auth(parameters: parameters, completion: completion)
        }
    }
    
    static func auth(parameters: [String: Any], completion: @escaping (LoginResponse) -> Void) {
        guard let url = URL(string: Globals.Server.host) else {
            return
        }
        let client = API(host: url).client(AuthClient.self)
        client.login(parameters: parameters, completion: completion)
    }
    
    static func logout(completion: @escaping () -> Void) {
            Realm.executeOnMainThread({ (realm) in
                realm.deleteAll()
            })
            completion()
    }

}
