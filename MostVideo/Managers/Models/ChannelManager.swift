//
//  ChannelManager.swift
//  MostVideo
//
//  Created by Roman on 16.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import Alamofire

struct ChannelManager {
    
    static func getChannelsList(completion: @escaping (Bool) -> Void) {
        API.current()?.fetch(ChannelRequest()) { response in
            switch response {
            case .resource(let resource):
                guard let status = resource.status, status == 200 else {
                    Alert(key: "error.get_channels_list").present()
                    completion(false)
                    return
                }
                Realm.executeOnMainThread({ (realm) in
                    realm.delete(realm.objects(Channel.self))
                    if let channels = resource.channels {
                        realm.add(channels.compactMap({ $0 }), update: .all)
                    }
                })
                completion(true)
            case .error:
                Alert(key: "error.get_channels_list").present()
                completion(false)
            }
        }
    }
}
