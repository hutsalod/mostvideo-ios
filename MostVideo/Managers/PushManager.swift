//
//  PushManager.swift
//  MostVideo
//
//  Created by Roman on 10.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import UserNotifications

final class PushManager {
    static let delegate = UserNotificationCenterDelegate()
    
    static let kDeviceTokenKey = "deviceToken"
    
    static func setupNotificationCenter() {
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.delegate = PushManager.delegate
        notificationCenter.requestAuthorization(options: [.alert, .sound]) { (_, _) in }
    }
    
    static func getDeviceToken() -> String? {
        guard let deviceToken = UserDefaults.group.string(forKey: kDeviceTokenKey) else {
            return nil
        }
        return deviceToken
    }
}


final class UserNotificationCenterDelegate: NSObject, UNUserNotificationCenterDelegate {
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response.notification.request.content.userInfo)
    }
}
