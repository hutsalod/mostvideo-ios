//
//  AppManager.swift
//  MostVideo
//
//  Created by Roman on 10.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation

struct AppManager {
    static func reloadApp() {
        DispatchQueue.main.async {
            if AuthManager.isAuthenticated() != nil {
                WindowManager.open(.main)
            } else {
                WindowManager.open(.auth)
            }
        }
    }

}
