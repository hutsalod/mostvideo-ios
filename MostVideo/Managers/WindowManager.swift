//
//  WindowManager.swift
//  MostVideo
//
//  Created by Roman on 13.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import UIKit

enum Storyboard {
    case auth
    case main
    
    var name: String {
        switch self {
        case .auth: return "Auth"
        case .main: return "Main"
        }
    }
    
    func instantiate() -> UIStoryboard {
        return UIStoryboard(name: name, bundle: Bundle.main)
    }
    
    func initialViewController() -> UIViewController? {
        let storyboard = instantiate()
        let controller = storyboard.instantiateInitialViewController()
        
        // preload view
        _ = controller?.view
        
        return controller
    }
    
    func instantiate(viewController: String) -> UIViewController? {
        let storyboard = instantiate()
        let controller = storyboard.instantiateViewController(withIdentifier: viewController)
        
        // preload view
        _ = controller.view

        return controller
    }
}

final class WindowManager {
    
    /**
     This method will transform the keyWindow.rootViewController
     into the initial view controller of storyboard with name param.
     
     - parameter name: The name of the Storyboard to be instantiated.
     - parameter transitionType: The transition to open new view controller.
     */
    static func open(_ storyboard: Storyboard, viewControllerIdentifier: String? = nil, transitionType: String = kCATransitionFade) {
           var controller: UIViewController?
           
           if let identifier = viewControllerIdentifier {
               controller = storyboard.instantiate(viewController: identifier)
           } else {
               controller = storyboard.initialViewController()
           }
        
        guard let window = UIApplication.shared.keyWindow else {
             return
         }
        
        window.rootViewController = controller
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        let duration: TimeInterval = 0.3

        UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
        { completed in
        })
       }
    
}
