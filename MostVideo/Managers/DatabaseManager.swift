//
//  DatabaseManager.swift
//  MostVideo
//
//  Created by Roman on 13.08.2018.
//  Copyright © 2018 MostVideo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

/**
 This keys are used to store all servers and
 database information to each server user is
 connected to.
 */
struct ServerPersistKeys {
    
    // Server controls
    static let authStorage = "kAuthStorage"

    // Authentication information
    static let token = "kAuthToken"
    static let serverURL = "kAuthServerURL"
    static let userId = "kUserId"

}

